@extends('main.index')
{{-- Head --}}
@push('style')
    <link rel="stylesheet" href="{{asset('assets/vendor/libs/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{asset('vendor/summernote/summernote-bs4.min.css')}}">
@endpush
@push('javascript')
    <script src="{{asset('assets/vendor/libs/select2/select2.js') }}"></script>
    <script src="{{asset('assets/js/forms-selects.js') }}"></script>
    <script src="{{asset('vendor/summernote/summernote-bs4.min.js')}}"></script>
    {{-- DATATABLE --}}
    <script type="text/javascript">
        let csrf = '{{ csrf_token() }}';
        var dt_user = $('.datatables-users').DataTable({
            order: [[0, 'desc']] ,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("index-berita") }}',
                type: 'GET',
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'judul', name: 'judul' },
                { data: 'kategori', name: 'kategori' },
                { data: 'pembaca', name: 'pembaca',searchable: false, orderable: false  },
                { data: 'status', name: 'status',searchable: false, orderable: false  },
                { data: 'actions', name: 'actions', orderable: false, searchable: false },
            ],
            createdRow: function (row, data, dataIndex) {
                $(row).find('.btn-delete').on('click', function() {
                    let beritaId = $(this).data('id');
                    Swal.fire({
                        title: 'Delete Data?',
                        text: 'Data will be Deleted!',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonText: '<i class="fas fa-check"></i> Delete',
                        cancelButtonText: '<i class="fas fa-times"></i> Cancel',
                        reverseButtons: true
                    })
                    .then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: "{{ route('berita.destroy', '') }}/" + beritaId, // Replace with the correct route URL
                                type: 'POST', // Use POST method for resource destroy
                                data: {
                                    _method: 'DELETE', // Specify the DELETE method using _method parameter
                                    _token: csrf,
                                },
                                success: function(response) {
                                    Swal.fire(
                                        'Success!',
                                        'Data Deleted!',
                                        'success'
                                    );
                                    $('.datatables-users').DataTable().ajax.reload();
                                },
                                error: function(xhr, status, error) {
                                    // Handle error response
                                    let errorMessage = '';
                                    if (xhr.responseJSON && xhr.responseJSON.message) {
                                        errorMessage = xhr.responseJSON.message;
                                    } else if (xhr.responseJSON && xhr.responseJSON.errors) {
                                        errorMessage = xhr.responseJSON.errors;
                                    } else {
                                        errorMessage = xhr.statusText;
                                    }
                                    Swal.fire({
                                        title: 'Oops...',
                                        text: errorMessage,
                                        icon: 'error',
                                        showConfirmButton: false,
                                        timer: 5000,
                                        timerProgressBar: true
                                    });
                                }
                            });
                        } else if (
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            Swal.fire(
                                'Canceled!',
                                'Undeleted Data!',
                                'warning'
                            )
                        }
                    });
                });
            }
        });
    </script>
    {{-- EDIT --}}
    <script>
        $(document).ready(function () {
            $(document).on('click', '.btn-edit', function() {
                let beritaId = $(this).data('id');
                let judul = $(this).data('judul');
                let publish = $(this).data('publish');
                let kategori = $(this).data('kategori');
                let konten = $(this).data('konten');

                $('#editBeritaId').val(beritaId);
                $('#editJudul').val(judul);
                $('#editPublish').val(publish).trigger('change.select2');
                $('#editKategori').val(kategori).trigger('change.select2');

                // Inisialisasi editor Summer Note
                $('#editsummernote').summernote();
                $('#editsummernote').summernote('code', atob(konten));
            });
            $('#EditBeritaModal').on('hidden.bs.modal', function () {
                $('#BeritaId').val('');
                $('#Editjudul').val('');
                $('#editPublish').val('').trigger('change');
                $('editKategori').val('').trigger('change');
                $('#summernote').summernote('code', '');
            });
        });
    </script>
    {{-- TEXT EDITOR --}}
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                onPaste: function (e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    document.execCommand('insertText', false, bufferText);
                }
            });
        });
    </script>
@endpush
{{-- Mark Sidebar --}}
@section('news.open', 'open')
@section('berita.active', 'active')
{{-- Mark Side Menu Sidebar --}}
@section('userlist.active', 'active')
{{-- --------------- --}}
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-semibold mb-4">Semua Berita</h4>

        <!-- Role cards -->
        <div class="row g-4">
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="card h-100">
                    <div class="row h-100">
                        <div class="col-sm-5">
                            <div class="d-flex align-items-end h-100 justify-content-center mt-sm-0 mt-3">
                                <img src="../../assets/img/illustrations/auth-register-illustration-dark.png"
                                    class="img-fluid mt-sm-4 mt-md-0" alt="add-new-roles" width="123" />
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body text-sm-end text-center ps-sm-0">
                                <button data-bs-target="#addBeritaModal" data-bs-toggle="modal"
                                class="btn btn-primary mb-2 text-nowrap add-new-role">
                                Tambah Berita
                            </button>
                            <p class="mb-0 mt-1">Berikan Informasi yang aktual</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <!-- Role Table -->
                <div class="card">
                    <div class="card-datatable table-responsive">
                        <table class="datatables-users table border-top">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Judul Berita</th>
                                    <th>Kategori</th>
                                    <th>Pembaca</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!--/ Role Table -->
            </div>
        </div>
        <div class="modal fade" id="addBeritaModal" tabindex="-1" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-fullscreen" role="document">
                <form id="formBerita" class="mb-3" action="{{ route('berita.store') }}" method="POST" enctype= "multipart/form-data">
                    @csrf
                    <div class="modal-content">
                        <div class="text-center mt-4">
                            <img src="{{ asset('assets/home/img/logo/logo_ub.png') }}" height="43" alt=""></a>
                            <img src="{{ asset('assets/home/img/logo/logo_arsip.png') }}" height="43" alt=""></a>
                            <h3 class="role-title mb-1">Tambah Berita</h3>
                            <p class="text-muted">Add some news to inform a good news</p>
                        </div>
                        <div class="modal-body">
                            <div class="card p-4">
                                <div class="mb-3">
                                    <label for="judul" class="form-label">Judul</label>
                                    <input type="text" class="form-control" id="judul" name="judul"
                                        placeholder="Masukkan Judul Berita" autofocus required/>
                                </div>
                                <div class="mb-3">
                                    <label for="kategori" class="form-label">Kategori</label>
                                    <select id="kategori" name="kategori_id" class="select2 form-select" required >
                                        <option value="" disabled selected></option>
                                        @foreach ($kategori as $kategoriOption )
                                        <option value="{{ $kategoriOption->id }}">{{ $kategoriOption->name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="publish" class="form-label">Status Berita</label>
                                    <select id="publish" name="publish" class="select2 form-select" required >
                                        <option value="" disabled selected></option>
                                        <option value="1">Publish</option>
                                        <option value="0">Draf</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="tumbnail" class="form-label">tumbnail</label>
                                    <input type="file" class="form-control" id="tumbnail" name="tumbnail" required/>
                                </div>
                                <div class="mb-3">
                                    <label for="full-editor" class="form-label">Konten Berita</label>
                                    <textarea id="summernote" name="newscontent" placeholder="Tuliskan berita disini" required>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-label-secondary waves-effect" data-bs-dismiss="modal">
                                Close
                            </button>
                            <button type="submit" class="btn btn-primary waves-effect waves-light swa-confirm-news">Simpan Berita</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        {{-- EDIT --}}
        <div class="modal fade" id="EditBeritaModal" tabindex="-1" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-fullscreen" role="document">
                <form id="formBerita" class="mb-3" action="{{ route('berita.update','1') }}" method="POST" enctype= "multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="modal-content">
                        <div class="text-center mt-4">
                            <img src="{{ asset('assets/home/img/logo/logo_ub.png') }}" height="43" alt=""></a>
                            <img src="{{ asset('assets/home/img/logo/logo_arsip.png') }}" height="43" alt=""></a>
                            <h3 class="role-title mb-1">Edit Berita</h3>
                            <p class="text-muted">Edit some news to inform a good news</p>
                        </div>
                        <div class="modal-body">
                            <div class="card p-4">
                                <div class="mb-3">
                                    <input type="hidden" name="berita_id" id="editBeritaId">
                                    <label for="editJudul" class="form-label">Judul</label>
                                    <input type="text" class="form-control" id="editJudul" name="judul"
                                        placeholder="Masukkan Judul Berita" autofocus required/>
                                </div>
                                <div class="mb-3">
                                    <label for="editKategori" class="form-label">Kategori</label>
                                    <select id="editKategori" name="kategori_id" class="select2 form-select" required >
                                        <option value="" disabled selected></option>
                                        @foreach ($kategori as $kategoriOption )
                                        <option value="{{ $kategoriOption->id }}">{{ $kategoriOption->name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="editPublish" class="form-label">Status Berita</label>
                                    <select id="editPublish" name="publish" class="select2 form-select" required >
                                        <option value="" disabled selected></option>
                                        <option value="1">Publish</option>
                                        <option value="0">Draf</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="tumbnail" class="form-label">tumbnail</label>
                                    <input type="file" class="form-control" id="tumbnail" name="tumbnail"/>
                                </div>
                                <div class="mb-3">
                                    <label for="full-editor" class="form-label">Konten Berita</label>
                                    <textarea id="editsummernote" name="newscontent" placeholder="Tuliskan berita disini" required>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-label-secondary waves-effect" data-bs-dismiss="modal">
                                Close
                            </button>
                            <button type="submit" class="btn btn-primary waves-effect waves-light swa-confirm-news">Update Berita</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection
