@extends('main.index')
{{-- Head --}}
@push('style')
    {{--  --}}
@endpush
@push('javascript')
    <script src="{{ asset('assets/js/app-access-roles.js') }}"></script>
    <script src="{{ asset('assets/js/modal-add-role.js') }}"></script>
    <script type="text/javascript">
        let csrf = '{{ csrf_token() }}';
        $(document).ready(function() {
            // Ketika checkbox all diubah statusnya
            $(document).on('change', '#selectAll', function() {
                // Ambil status checked/unchecked dari checkbox all
                let isChecked = $(this).is(':checked');
                // Ubah status checked/unchecked pada semua checkbox dengan class "checkbox"
                $('.form-check-input').prop('checked', isChecked);
            });

            // Ketika salah satu checkbox dengan class "checkbox" diubah statusnya
            $(document).on('change', '.form-check-input', function() {
                // Periksa jumlah checkbox yang sudah tercentang
                let checkedCount = $('.form-check-input:checked').length;
                let totalCount = $('.form-check-input').length;

                // Jika semua checkbox tercentang, centang juga checkbox all
                $('#selectAll').prop('checked', checkedCount === totalCount);
            });
        });
    </script>
@endpush
{{-- Mark Sidebar --}}
@section('news.open', 'open')
{{-- Mark Side Menu Sidebar --}}
@section('kategoriberita.active', 'active')
{{-- --------------- --}}
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-semibold mb-4">List Kategori Berita</h4>
        <p class="mb-4">
            Kategori Berita-Berita yang terpublish
        </p>
        <!-- Role cards -->
        <div class="row g-4">
            @foreach ($kategoriall as $kategori)
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <h6 class="fw-normal mb-2">Total
                                    {{ $kategori->berita->count() }}
                                    Berita Ter-publish</h6>
                            </div>
                            <div class="d-flex justify-content-between align-items-end mt-1">
                                <div class="role-heading">
                                    <h4 class="mb-1">{{ $kategori->name }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="card h-100">
                    <div class="row h-100">
                        <div class="col-sm-5">
                            <div class="d-flex align-items-end h-100 justify-content-center mt-sm-0 mt-3">
                                <img src="../../assets/img/illustrations/auth-verify-email-illustration-dark.png"
                                    class="img-fluid mt-sm-4 mt-md-0" alt="add-new-roles" width="83" />
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body text-sm-end text-center ps-sm-0">
                                <button data-bs-target="#addRoleModal" data-bs-toggle="modal"
                                    class="btn btn-primary mb-2 text-nowrap add-new-role">
                                    Kategori Berita
                                </button>
                                <p class="mb-0 mt-1">Kategori untuk masing-masing Berita</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <!-- Role Table -->
                <div class="card">
                    <div class="card-datatable table-responsive">
                        <table class="datatables-users table border-top">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>id</th>
                                    <th>Kategori Name</th>
                                    <th>Total Berita</th>
                                    {{-- <th>Actions</th> --}}
                                    {{--
                                        action diserahkan ke masing-masing programmer berdasarkan kebutuhan pengembangan aplikasi
                                        --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($kategoriall as $kategorishow)
                                    <tr>
                                        <td></td>
                                        <td>{{ $kategorishow->id }}</td>
                                        <td>{{ $kategorishow->name }}</td>
                                        <td>{{ $kategorishow->berita->count() }}</td>
                                        {{-- <td></td> --}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--/ Role Table -->
            </div>
        </div>
        <!--/ Role cards -->
        <!-- Add Role Modal -->
        <div class="modal fade" id="addRoleModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-add-new-role">
                <div class="modal-content p-3 p-md-5">
                    <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body">
                        <div class="text-center mb-4">
                            <h3 class="role-title mb-2">Tambah Kategori Baru</h3>
                            <p class="text-muted">add new category for news</p>
                        </div>
                        <!-- Add role form -->
                    <form id="addRoleForm" class="row g-3" action="{{ route('kategori-berita.store') }}" method="POST">
                            @csrf
                            <div class="col-12 mb-4">
                                <label class="form-label" for="modalRoleName">Nama kategori</label>
                                <input type="text" id="modalRoleName" name="kategori" class="form-control"
                                    placeholder="Masukan nama kategori baru" tabindex="-1" />
                            </div>
                            <div class="col-12 text-center mt-4">
                                <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                                <button type="reset" class="btn btn-label-secondary" data-bs-dismiss="modal"
                                    aria-label="Close">
                                    Cancel
                                </button>
                            </div>
                        </form>
                        <!--/ Add role form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
