@extends('main.index')
{{-- Head --}}
@push('style')
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('vendor/summernote/summernote-bs4.min.css') }}">
@endpush
@push('javascript')
    <script src="{{ asset('assets/vendor/libs/select2/select2.js') }}"></script>
    <script src="{{ asset('assets/js/forms-selects.js') }}"></script>
    <script src="{{ asset('vendor/summernote/summernote-bs4.min.js') }}"></script>
    {{-- DATATABLE --}}
    <script type="text/javascript">
        let csrf = '{{ csrf_token() }}';
        var dt_user = $('.datatables-kategori').DataTable({
                    order: [
                        [0, 'desc']
                    ],
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: '{{ route('kategori-dokumen-management.create') }}',
                        type: 'GET',
                    },
                        columns: [{
                                data: 'DT_RowIndex',
                                orderable: false,
                                searchable: false
                            },
                            {
                                data: 'name',
                                name: 'name'
                            },
                            {
                                data: 'actions',
                                name: 'actions',
                                orderable: false,
                                searchable: false
                            },
                        ],
                        createdRow: function(row, data, dataIndex) {
                            $(row).find('.btn-delete').on('click', function() {
                                let KategoriId = $(this).data('id');
                                Swal.fire({
                                        title: 'Delete Data?',
                                        text: 'Data will be Deleted!',
                                        icon: 'question',
                                        showCancelButton: true,
                                        confirmButtonText: '<i class="fas fa-check"></i> Delete',
                                        cancelButtonText: '<i class="fas fa-times"></i> Cancel',
                                        reverseButtons: true
                                    })
                                    .then((result) => {
                                        if (result.isConfirmed) {
                                            $.ajax({
                                                url: "{{ route('kategori-dokumen-management.destroy', '') }}/" +
                                                    KategoriId, // Replace with the correct route URL
                                                type: 'POST', // Use POST method for resource destroy
                                                data: {
                                                    _method: 'DELETE', // Specify the DELETE method using _method parameter
                                                    _token: csrf,
                                                },
                                                success: function(response) {
                                                    Swal.fire(
                                                        'Success!',
                                                        'Data Deleted!',
                                                        'success'
                                                    );
                                                    $('.datatables-kategori').DataTable().ajax
                                                        .reload();
                                                },
                                                error: function(xhr, status, error) {
                                                    // Handle error response
                                                    let errorMessage = '';
                                                    if (xhr.responseJSON && xhr.responseJSON
                                                        .message) {
                                                        errorMessage = xhr.responseJSON.message;
                                                    } else if (xhr.responseJSON && xhr
                                                        .responseJSON.errors) {
                                                        errorMessage = xhr.responseJSON.errors;
                                                    } else {
                                                        errorMessage = xhr.statusText;
                                                    }
                                                    Swal.fire({
                                                        title: 'Oops...',
                                                        text: errorMessage,
                                                        icon: 'error',
                                                        showConfirmButton: false,
                                                        timer: 5000,
                                                        timerProgressBar: true
                                                    });
                                                }
                                            });
                                        } else if (
                                            result.dismiss === Swal.DismissReason.cancel
                                        ) {
                                            Swal.fire(
                                                'Canceled!',
                                                'Undeleted Data!',
                                                'warning'
                                            )
                                        }
                                    });
                            });
                        }
                    });
    </script>
@endpush
{{-- Mark Sidebar --}}
@section('document.open', 'open')
@section('document.active', 'active')
{{-- Mark Side Menu Sidebar --}}
@section('documentKategori.active', 'active')
{{-- --------------- --}}
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-semibold mb-4">Semua Kategori</h4>

        <!-- Role cards -->
        <div class="row g-4">
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="card h-100">
                    <div class="row h-100">
                        <div class="col-sm-5">
                            <div class="d-flex align-items-end h-100 justify-content-center mt-sm-0 mt-3">
                                <img src="../../assets/img/illustrations/auth-register-illustration-dark.png"
                                    class="img-fluid mt-sm-4 mt-md-0" alt="add-new-roles" width="123" />
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body text-sm-end text-center ps-sm-0">
                                <button data-bs-target="#addKategoriModal" data-bs-toggle="modal"
                                    class="btn btn-primary mb-2 text-nowrap add-new-role">
                                    Tambah Kategori
                                </button>
                                <p class="mb-0 mt-1">Berikan Kategori terupdate</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <!-- Role Table -->
                <div class="card">
                    <div class="card-datatable table-responsive">
                        <table class="datatables-kategori table border-top">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Kategori</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!--/ Role Table -->
            </div>
        </div>
        <div class="modal fade" id="addKategoriModal" tabindex="-1" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-fullscreen" role="document">
                <form id="formKategori" class="mb-3" action="{{ route('kategori-dokumen-management.store') }}" method="POST"
                    enctype= "multipart/form-data">
                    @csrf
                    <div class="modal-content">
                        <div class="text-center mt-4">
                            <img src="{{ asset('assets/home/img/logo/logo_ub.png') }}" height="43" alt=""></a>
                            <img src="{{ asset('assets/home/img/logo/logo_arsip.png') }}" height="43"
                                alt=""></a>
                            <h3 class="role-title mb-1">Tambah Kategori</h3>
                            <p class="text-muted">Add some news to inform a good news</p>
                        </div>
                        <div class="modal-body">
                            <div class="card p-4">
                                <div class="mb-3">
                                    <label for="judul" class="form-label">Judul</label>
                                    <input type="text" class="form-control" id="judul" name="judul"
                                        placeholder="Masukkan Kategori" autofocus required />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-label-secondary waves-effect" data-bs-dismiss="modal">
                                Close
                            </button>
                            <button type="submit" class="btn btn-primary waves-effect waves-light swa-confirm-news">Simpan
                                Kategori</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endsection
