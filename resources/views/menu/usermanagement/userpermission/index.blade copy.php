@extends('main.index')
{{-- Head --}}
@push('style')
    {{--  --}}
@endpush
@push('javascript')
    <script src="{{ asset('assets/vendor/libs/dropzone/dropzone.js') }}"></script>
    <script src="{{ asset('assets/js/app-access-roles.js') }}"></script>
    <script src="{{ asset('assets/js/modal-add-role.js') }}"></script>
    <script type="text/javascript">
        let csrf = '{{ csrf_token() }}';
        $(document).ready(function() {
            // Ketika checkbox all diubah statusnya
            $(document).on('change', '#selectAll', function() {
                // Ambil status checked/unchecked dari checkbox all
                let isChecked = $(this).is(':checked');
                // Ubah status checked/unchecked pada semua checkbox dengan class "checkbox"
                $('.form-check-input').prop('checked', isChecked);
            });

            // Ketika salah satu checkbox dengan class "checkbox" diubah statusnya
            $(document).on('change', '.form-check-input', function() {
                // Periksa jumlah checkbox yang sudah tercentang
                let checkedCount = $('.form-check-input:checked').length;
                let totalCount = $('.form-check-input').length;

                // Jika semua checkbox tercentang, centang juga checkbox all
                $('#selectAll').prop('checked', checkedCount === totalCount);
            });
        });
    </script>
@endpush
{{-- Mark Sidebar --}}
@section('usermanagement.open', 'open')
@section('usermanagement.active', 'active')
{{-- Mark Side Menu Sidebar --}}
@section('userrole.active', 'active')
{{-- --------------- --}}
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-semibold mb-4">User All List</h4>
        <p class="mb-4">
            Company yang terdaftar pada MerdekaBaseApps <br />
            Penambahan Company untuk grouping user berdasarkan company.
        </p>
        <!-- Role cards -->
        <div class="row g-4">
            @foreach ($role as $menurole)
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <h6 class="fw-normal mb-2">Total
                                    {{ $menurole->users->count() }}
                                    users</h6>
                                <ul class="list-unstyled d-flex align-items-center avatar-group mb-0">
                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                        title="Vinnie Mostowy" class="avatar avatar-sm pull-up">
                                        <img class="rounded-circle" src="../../assets/img/avatars/5.png" alt="Avatar" />
                                    </li>
                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                        title="Allen Rieske" class="avatar avatar-sm pull-up">
                                        <img class="rounded-circle" src="../../assets/img/avatars/12.png" alt="Avatar" />
                                    </li>
                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                        title="Julee Rossignol" class="avatar avatar-sm pull-up">
                                        <img class="rounded-circle" src="../../assets/img/avatars/6.png" alt="Avatar" />
                                    </li>
                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                        title="Kaith D'souza" class="avatar avatar-sm pull-up">
                                        <img class="rounded-circle" src="../../assets/img/avatars/3.png" alt="Avatar" />
                                    </li>
                                    <li data-bs-toggle="tooltip" data-popup="tooltip-custom" data-bs-placement="top"
                                        title="John Doe" class="avatar avatar-sm pull-up">
                                        <img class="rounded-circle" src="../../assets/img/avatars/1.png" alt="Avatar" />
                                    </li>
                                </ul>
                            </div>
                            <div class="d-flex justify-content-between align-items-end mt-1">
                                <div class="role-heading">
                                    <h4 class="mb-1">{{ $menurole->name }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="card h-100">
                    <div class="row h-100">
                        <div class="col-sm-5">
                            <div class="d-flex align-items-end h-100 justify-content-center mt-sm-0 mt-3">
                                <img src="../../assets/img/illustrations/auth-verify-email-illustration-dark.png"
                                    class="img-fluid mt-sm-4 mt-md-0" alt="add-new-roles" width="83" />
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body text-sm-end text-center ps-sm-0">
                                <button data-bs-target="#addRoleModal" data-bs-toggle="modal"
                                    class="btn btn-primary mb-2 text-nowrap add-new-role">
                                    Tambah Role
                                </button>
                                <p class="mb-0 mt-1">Tambah Role untuk Role user</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <!-- Role Table -->
                <div class="card">
                    <div class="card-datatable table-responsive">
                        <table class="datatables-users table border-top">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>id</th>
                                    <th>Role Name</th>
                                    <th>Total User</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($role as $detilrole)
                                    <tr>
                                        <td></td>
                                        <td>{{ $detilrole->id }}</td>
                                        <td>{{ $detilrole->name }}</td>
                                        <td>{{ $detilrole->users->count() }}</td>
                                        <td>action</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!--/ Role Table -->
            </div>
        </div>
        <!--/ Role cards -->

        <!-- Add Role Modal -->
        <!-- Add Role Modal -->
        <div class="modal fade" id="addRoleModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-add-new-role">
                <div class="modal-content p-3 p-md-5">
                    <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body">
                        <div class="text-center mb-4">
                            <h3 class="role-title mb-2">Add New Role</h3>
                            <p class="text-muted">Set role permissions</p>
                        </div>
                        <!-- Add role form -->

                        <form id="addRoleForm" class="row g-3" action="{{ route('user-role.store') }}" method="POST">
                            @csrf
                            <div class="col-12 mb-4">
                                <label class="form-label" for="modalRoleName">Role Name</label>
                                <input type="text" id="modalRoleName" name="rolename" class="form-control"
                                    placeholder="Enter a role name" tabindex="-1" />
                            </div>
                            <div class="col-12">
                                <h5>Role Permissions</h5>
                                <!-- Permission table -->
                                <div class="table-responsive">
                                    <table class="table table-flush-spacing">
                                        <tbody>
                                            <tr>
                                                <td class="text-nowrap fw-semibold">
                                                    Special Access
                                                    <i class="ti ti-info-circle" data-bs-toggle="tooltip"
                                                        data-bs-placement="top"
                                                        title="Allows a full access to the system"></i>
                                                </td>
                                                <td>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox" id="selectAll" />
                                                        <label class="form-check-label" for="selectAll"> Select All
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            @foreach ($submenu as $key => $sub)
                                                <tr>
                                                    <td class="text-nowrap fw-semibold">{{ $sub->title }}</td>
                                                    <td>
                                                        <div class="d-flex">
                                                            <div class="form-check me-3 me-lg-5">
                                                                <label class="form-check-label"
                                                                    for="userManagementRead{{ $key }}"> Read
                                                                </label>
                                                                <input class="form-check-input" name="permissions[]"
                                                                    type="checkbox"
                                                                    id="userManagementRead{{ $key }}"
                                                                    value="{{ $sub->id }}:read" />
                                                            </div>
                                                            <div class="form-check me-3 me-lg-5">
                                                                <label class="form-check-label"
                                                                    for="userManagementUpdate{{ $key }}"> Update
                                                                </label>
                                                                <input class="form-check-input" name="permissions[]"
                                                                    type="checkbox"
                                                                    id="userManagementUpdate{{ $key }}"
                                                                    value="{{ $sub->id }}:update" />
                                                            </div>
                                                            <div class="form-check me-3 me-lg-5">
                                                                <label class="form-check-label"
                                                                    for="userManagementCreate{{ $key }}"> Create
                                                                </label>
                                                                <input class="form-check-input" name="permissions[]"
                                                                    type="checkbox"
                                                                    id="userManagementCreate{{ $key }}"
                                                                    value="{{ $sub->id }}:create" />
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="form-check-label"
                                                                    for="userManagementDelete{{ $key }}"> Delete
                                                                </label>
                                                                <input class="form-check-input" name="permissions[]"
                                                                    type="checkbox"
                                                                    id="userManagementDelete{{ $key }}"
                                                                    value="{{ $sub->id }}:delete" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                <!-- Permission table -->
                            </div>
                            <div class="col-12 text-center mt-4">
                                <button type="submit" class="btn btn-primary me-sm-3 me-1">Submit</button>
                                <button type="reset" class="btn btn-label-secondary" data-bs-dismiss="modal"
                                    aria-label="Close">
                                    Cancel
                                </button>
                            </div>
                        </form>
                        <!--/ Add role form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
