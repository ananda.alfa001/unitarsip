@extends('main.index')
{{-- Head --}}
@push('style')
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/select2/select2.css') }}" />
@endpush
@push('javascript')
    <script src="{{ asset('assets/vendor/libs/select2/select2.js') }}"></script>
    <script src="{{ asset('assets/js/forms-selects.js') }}"></script>
    <script type="text/javascript">
        let csrf = '{{ csrf_token() }}';
        var dt_user = $('.datatables-users').DataTable({
            order: [[0, 'desc']] ,
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("index-user") }}',
                type: 'GET',
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'roles', name: 'roles',searchable: false, orderable: false  },
                { data: 'actions', name: 'actions', orderable: false, searchable: false },
            ],
            createdRow: function (row, data, dataIndex) {
                $(row).find('.btn-delete').on('click', function() {
                    var userId = $(this).data('id');
                    Swal.fire({
                        title: 'Delete Data?',
                        text: 'Data will be Deleted!',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonText: '<i class="fas fa-check"></i> Delete',
                        cancelButtonText: '<i class="fas fa-times"></i> Cancel',
                        reverseButtons: true
                    })
                    .then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: "{{ route('user-list.destroy', '') }}/" + userId, // Replace with the correct route URL
                                type: 'POST', // Use POST method for resource destroy
                                data: {
                                    _method: 'DELETE', // Specify the DELETE method using _method parameter
                                    _token: csrf,
                                },
                                success: function(response) {
                                    Swal.fire(
                                        'Success!',
                                        'Data Deleted!',
                                        'success'
                                    );
                                    $('.datatables-users').DataTable().ajax.reload();
                                },
                                error: function(xhr, status, error) {
                                    // Handle error response
                                    let errorMessage = '';
                                    if (xhr.responseJSON && xhr.responseJSON.message) {
                                        errorMessage = xhr.responseJSON.message;
                                    } else if (xhr.responseJSON && xhr.responseJSON.errors) {
                                        errorMessage = xhr.responseJSON.errors;
                                    } else {
                                        errorMessage = xhr.statusText;
                                    }
                                    Swal.fire({
                                        title: 'Oops...',
                                        text: errorMessage,
                                        icon: 'error',
                                        showConfirmButton: false,
                                        timer: 5000,
                                        timerProgressBar: true
                                    });
                                }
                            });
                        } else if (
                            result.dismiss === Swal.DismissReason.cancel
                        ) {
                            Swal.fire(
                                'Canceled!',
                                'Undeleted Data!',
                                'warning'
                            )
                        }
                    });
                });
            }
        });
    </script>
    <script>
        $(document).ready(function () {
            $(document).on('click', '.btn-edit', function() {
                let userId = $(this).data('id');
                let userName = $(this).data('name');
                let userEmail = $(this).data('email');
                let userRoles = $(this).data('roles');
                $('#edit-userid').val(userId);
                $('#edit-name').val(userName);
                $('#edit-email').val(userEmail);
                $('#edit-roles').val(userRoles).trigger('change.select2');
            });
            $('#editUserModal').on('hidden.bs.modal', function () {
                $('#edit-userid').val('');
                $('#edit-name').val('');
                $('#edit-email').val('');
                $('#edit-roles').val('').trigger('change');
            });
        });
    </script>
@endpush
{{-- Mark Sidebar --}}
@section('usermanagement.open', 'open')
@section('usermanagement.active', 'active')
{{-- Mark Side Menu Sidebar --}}
@section('userlist.active', 'active')
{{-- --------------- --}}
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-semibold mb-4">User All List</h4>

        <!-- Role cards -->
        <div class="row g-4">
            <div class="col-xl-4 col-lg-6 col-md-6">
                <div class="card h-100">
                    <div class="row h-100">
                        <div class="col-sm-5">
                            <div class="d-flex align-items-end h-100 justify-content-center mt-sm-0 mt-3">
                                <img src="../../assets/img/illustrations/auth-register-illustration-dark.png"
                                    class="img-fluid mt-sm-4 mt-md-0" alt="add-new-roles" width="123" />
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="card-body text-sm-end text-center ps-sm-0">
                                <button data-bs-target="#addUserModal" data-bs-toggle="modal"
                                class="btn btn-primary mb-2 text-nowrap add-new-role">
                                Tambah User
                            </button>
                            <p class="mb-0 mt-1">Tambah User</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <!-- Role Table -->
                <div class="card">
                    <div class="card-datatable table-responsive">
                        <table class="datatables-users table border-top">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!--/ Role Table -->
            </div>
        </div>
        <!--/ Role cards -->
        <!-- Add Role Modal -->
        <div class="modal fade" id="addUserModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-add-new-role">
                <div class="modal-content p-3 p-md-5">
                    <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                    <div class="modal-body">
                        <div class="text-center mb-4">
                            <h3 class="role-title mb-2">Tambah User</h3>
                            <p class="text-muted">Set new User</p>
                        </div>
                        <!-- Add role form -->
                        <form id="formAuthentication" class="mb-3" action="{{ route('user-list.store') }}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">Name</label>
                                <input type="text" class="form-control" id="username" name="name"
                                    placeholder="Enter your Name" autofocus required/>
                            </div>
                            <div class="mb-3">
                                <div class="col-md-12 mb-3">
                                    <label for="role" class="form-label">Role User</label>
                                    <select id="role" name="role_id" class="select2 form-select" required>
                                        <option value="" disabled selected></option>
                                        @foreach ($role as $roleOption )
                                        <option value="{{ $roleOption->id }}">{{ $roleOption->name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="text" class="form-control" id="email" name="email"
                                    placeholder="Enter your email" required/>
                            </div>
                            <div class="mb-3 form-password-toggle">
                                <label class="form-label" for="password">Password</label>
                                <div class="input-group input-group-merge">
                                    <input type="password" id="password" class="form-control" name="password"
                                        placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                        aria-describedby="password" required/>
                                    <span class="input-group-text cursor-pointer"><i class="ti ti-eye-off"></i></span>
                                </div>
                            </div>

                            <div class="mb-3">
                            </div>
                            <button class="btn btn-primary d-grid w-100">Create User</button>
                        </form>
                        <!--/ Add role form -->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editUserModal" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered modal-add-new-role">
                <div class="modal-content p-3 p-md-5">
                    <button type="button" class="btn-close btn-pinned" data-bs-dismiss="modal"
                        aria-label="Close"></button>
                    <div class="modal-body">
                        <div class="text-center mb-4">
                            <h3 class="role-title mb-2">Edit User</h3>
                            <p class="text-muted">Edit User</p>
                        </div>
                        <!-- Add role form -->
                        <form id="formAuthentication" class="mb-3" action="{{ route('user-list.update','1') }}" method="POST">
                            @method('PUT')
                            @csrf
                            <input type="hidden" name="user_id" id="edit-userid">
                            <div class="mb-3">
                                <label for="edit-name" class="form-label">Name</label>
                                <input type="text" class="form-control" id="edit-name" name="name"
                                    placeholder="Enter your Name" autofocus required/>
                            </div>
                            <div class="mb-3">
                                <div class="col-md-12 mb-3">
                                    <label for="edit-roles" class="form-label">Role User</label>
                                    <select id="edit-roles" name="role_id" class="select2 form-select" required>
                                        <option value="" disabled selected></option>
                                        @foreach ($role as $roleOption )
                                        <option value="{{ $roleOption->id }}">{{ $roleOption->name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="edit-email" class="form-label">Email</label>
                                <input type="text" class="form-control" id="edit-email" name="email"
                                    placeholder="Enter your email" required/>
                            </div>
                            <div class="mb-3 form-password-toggle">
                                <label class="form-label" for="password">Password</label>
                                <div class="input-group input-group-merge">
                                    <input type="password" id="password" class="form-control" name="password"
                                        placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                        aria-describedby="password"/>
                                    <span class="input-group-text cursor-pointer"><i class="ti ti-eye-off"></i></span>
                                </div>
                            </div>

                            <div class="mb-3">
                            </div>
                            <button class="btn btn-primary d-grid w-100">Update User</button>
                        </form>
                        <!--/ Add role form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
