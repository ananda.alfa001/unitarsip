@extends('main.index')
{{-- Head --}}
@push('style')
@endpush
@push('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/exceljs/4.3.0/exceljs.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.5/FileSaver.min.js"></script>

    {{-- DATATABLE --}}
    <script type="text/javascript">
        let csrf = '{{ csrf_token() }}';
        let dt_user = $('.datatables-users').DataTable({
            order: [
                [0, 'desc']
            ],
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route('survey-management.create') }}',
                type: 'GET',
            },
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'nama',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'asal',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'email',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'no_hp',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan1',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan2',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan3',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan4',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan5',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan6',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan7',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan8',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan9',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan10',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan11',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan12',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan13',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'pertanyaan14',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'actions',
                    name: 'actions',
                    orderable: false,
                    searchable: false
                },
            ],
            createdRow: function(row, data, dataIndex) {
                $(row).find('.btn-delete').on('click', function() {
                    let DokumenId = $(this).data('id');
                    Swal.fire({
                            title: 'Delete Data?',
                            text: 'Data will be Deleted!',
                            icon: 'question',
                            showCancelButton: true,
                            confirmButtonText: '<i class="fas fa-check"></i> Delete',
                            cancelButtonText: '<i class="fas fa-times"></i> Cancel',
                            reverseButtons: true
                        })
                        .then((result) => {
                            if (result.isConfirmed) {
                                $.ajax({
                                    url: "{{ route('survey-management.destroy', '') }}/" +
                                        DokumenId, // Replace with the correct route URL
                                    type: 'POST', // Use POST method for resource destroy
                                    data: {
                                        _method: 'DELETE', // Specify the DELETE method using _method parameter
                                        _token: csrf,
                                    },
                                    success: function(response) {
                                        Swal.fire(
                                            'Success!',
                                            'Data Deleted!',
                                            'success'
                                        );
                                        $('.datatables-users').DataTable().ajax
                                            .reload();
                                    },
                                    error: function(xhr, status, error) {
                                        // Handle error response
                                        let errorMessage = '';
                                        if (xhr.responseJSON && xhr.responseJSON
                                            .message) {
                                            errorMessage = xhr.responseJSON.message;
                                        } else if (xhr.responseJSON && xhr
                                            .responseJSON.errors) {
                                            errorMessage = xhr.responseJSON.errors;
                                        } else {
                                            errorMessage = xhr.statusText;
                                        }
                                        Swal.fire({
                                            title: 'Oops...',
                                            text: errorMessage,
                                            icon: 'error',
                                            showConfirmButton: false,
                                            timer: 5000,
                                            timerProgressBar: true
                                        });
                                    }
                                });
                            } else if (
                                result.dismiss === Swal.DismissReason.cancel
                            ) {
                                Swal.fire(
                                    'Canceled!',
                                    'Undeleted Data!',
                                    'warning'
                                )
                            }
                        });
                });
            }
        });

        $(document).ready(function() {
            // Inisialisasi DataTable Anda di sini

            // Fungsi untuk menghasilkan file Excel
            function generateExcel() {
                var workbook = new ExcelJS.Workbook();
                var worksheet = workbook.addWorksheet('Sheet1');

                // Tambahkan header kolom
                $('#surveytable thead th').each(function(index, element) {
                    worksheet.getCell(1, index + 1).value = $(element).text();
                });

                // Tambahkan data
                $('#surveytable tbody tr').each(function(rowIndex, row) {
                    $(row).find('td').each(function(colIndex, cell) {
                        worksheet.getCell(rowIndex + 2, colIndex + 1).value = $(cell).text();
                    });
                });

                // Simpan workbook ke file Excel
                workbook.xlsx.writeBuffer().then(function(buffer) {
                    saveAs(new Blob([buffer]), 'Data Survey Arsip.xlsx');
                });
            }

            // Tambahkan tombol untuk menghasilkan file Excel
            $('#generateExcelBtn').on('click', function() {
                generateExcel();
            });
        });
    </script>
@endpush
{{-- Mark Sidebar --}}
@section('survey.open', 'open')
@section('survey.active', 'active')
{{-- Mark Side Menu Sidebar --}}
@section('surveymanage.active', 'active')
{{-- --------------- --}}
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-semibold mb-4">Data Survey Arsip</h4>
        <!-- Role cards -->
        <div class="row g-4">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="card h-100">
                    <div class="row h-100">
                        <div class="col-sm-2">
                            <div class="d-flex align-items-center justify-content-center h-100">
                                <img src="../../assets/img/illustrations/auth-register-illustration-dark.png"
                                    class="img-fluid" alt="add-new-roles" width="143" />
                            </div>
                        </div>
                        <div class="col-sm-10 pt-4 pb-4">
                            <h4>List Pertanyaan Survey</h4>
                            <ul>
                                @foreach ($pertanyaan as $key => $data)
                                    <li>Question {{ $key + 1 }} : {{ $data->pertanyaan }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12">
                <!-- Role Table -->
                <div class="card">
                    <div class="card-datatable table-responsive">
                        <table id="surveytable" class="datatables-users table border-top">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nama</th>
                                    <th>Asal</th>
                                    <th>Email</th>
                                    <th>No Telp.</th>
                                    <th>Question 1</th>
                                    <th>Question 2</th>
                                    <th>Question 3</th>
                                    <th>Question 4</th>
                                    <th>Question 5</th>
                                    <th>Question 6</th>
                                    <th>Question 7</th>
                                    <th>Question 8</th>
                                    <th>Question 9</th>
                                    <th>Question 10</th>
                                    <th>Question 11</th>
                                    <th>Question 12</th>
                                    <th>Question 13</th>
                                    <th>Kritik Saran</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!--/ Role Table -->
                <button class="btn btn-block btn-success w-100" id="generateExcelBtn">Download Survey</button>
            </div>
        </div>
    @endsection
