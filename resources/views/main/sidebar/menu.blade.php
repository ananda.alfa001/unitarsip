
<ul class="menu-inner py-1">

    <li class="menu-item @yield('news.active') @yield('news.open')">
        <a href="javascript:void(0);" class="menu-link menu-toggle">
            {{-- SVG Icon Tabler --}}
            <svg xmlns="http://www.w3.org/2000/svg" class="icon menu-icon icon-tabler icon-tabler-user-edit" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M8 7a4 4 0 1 0 8 0a4 4 0 0 0 -8 0"></path>
                <path d="M6 21v-2a4 4 0 0 1 4 -4h3.5"></path>
                <path d="M18.42 15.61a2.1 2.1 0 0 1 2.97 2.97l-3.39 3.42h-3v-3l3.42 -3.39z"></path>
            </svg>
            <div data-i18n="Berita">Berita</div>
        </a>
        <ul class="menu-sub">
            <li class="menu-item @yield('berita.active')">
                <a href="{{route('berita.index')}}" class="menu-link">
                    <div data-i18n="Update Berita">Update Berita</div>
                </a>
            </li>
            <li class="menu-item @yield('kategoriberita.active')">
                <a href="{{route('kategori-berita.index')}}" class="menu-link">
                    <div data-i18n="Update Kategori Berita">Update Kategori Berita</div>
                </a>
            </li>
        </ul>
    </li>

    <li class="menu-item @yield('document.active') @yield('document.open')">
        <a href="javascript:void(0);" class="menu-link menu-toggle">
            {{-- SVG Icon Tabler --}}
            <svg xmlns="http://www.w3.org/2000/svg" class="icon menu-icon icon-tabler icon-tabler-user-edit" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M8 7a4 4 0 1 0 8 0a4 4 0 0 0 -8 0"></path>
                <path d="M6 21v-2a4 4 0 0 1 4 -4h3.5"></path>
                <path d="M18.42 15.61a2.1 2.1 0 0 1 2.97 2.97l-3.39 3.42h-3v-3l3.42 -3.39z"></path>
            </svg>
            <div data-i18n="Berita">Dokumen Publikasi</div>
        </a>
        <ul class="menu-sub">
            <li class="menu-item @yield('documentList.active')">
                <a href="{{route('dokumen-management.index')}}" class="menu-link">
                    <div data-i18n="Update Dokumen">Update Dokumen</div>
                </a>
            </li>
            <li class="menu-item @yield('documentKategori.active')">
                <a href="{{route('kategori-dokumen-management.index')}}" class="menu-link">
                    <div data-i18n="Update Kategori Dokumen">Update Kategori Dokumen</div>
                </a>
            </li>
        </ul>
    </li>

    <li class="menu-item @yield('survey.active') @yield('survey.open')">
        <a href="javascript:void(0);" class="menu-link menu-toggle">
            {{-- SVG Icon Tabler --}}
            <svg xmlns="http://www.w3.org/2000/svg" class="icon menu-icon icon-tabler icon-tabler-user-edit" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M8 7a4 4 0 1 0 8 0a4 4 0 0 0 -8 0"></path>
                <path d="M6 21v-2a4 4 0 0 1 4 -4h3.5"></path>
                <path d="M18.42 15.61a2.1 2.1 0 0 1 2.97 2.97l-3.39 3.42h-3v-3l3.42 -3.39z"></path>
            </svg>
            <div data-i18n="Berita">Survey Arsip</div>
        </a>
        <ul class="menu-sub">
            <li class="menu-item @yield('surveymanage.active')">
                <a href="{{route('survey-management.index')}}" class="menu-link">
                    <div data-i18n="Update Dokumen">Data Survey</div>
                </a>
            </li>
        </ul>
    </li>

    <li class="menu-item @yield('usermanagement.active') @yield('usermanagement.open')">
        <a href="javascript:void(0);" class="menu-link menu-toggle">
            {{-- SVG Icon Tabler --}}
            <svg xmlns="http://www.w3.org/2000/svg" class="icon menu-icon icon-tabler icon-tabler-user-edit" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <path d="M8 7a4 4 0 1 0 8 0a4 4 0 0 0 -8 0"></path>
                <path d="M6 21v-2a4 4 0 0 1 4 -4h3.5"></path>
                <path d="M18.42 15.61a2.1 2.1 0 0 1 2.97 2.97l-3.39 3.42h-3v-3l3.42 -3.39z"></path>
            </svg>
            <div data-i18n="User Management">User Management</div>
        </a>
        <ul class="menu-sub">
            <li class="menu-item @yield('userlist.active')">
                <a href="{{route('user-list.index')}}" class="menu-link">
                    <div data-i18n="List User">List User</div>
                </a>
            </li>
            <li class="menu-item @yield('userrole.active')">
                <a href="{{route('user-role.index')}}" class="menu-link">
                    <div data-i18n="User Role">User Role</div>
                </a>
            </li>
        </ul>
    </li>
</ul>
