  <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="index.html" class="app-brand-link">
            <span class="app-brand-logo">
                <img src="{{ asset('assets/home/img/logo/logo_arsip1.png') }}" height="43" alt=""></a>
            </span>
            <span class="app-brand-text demo menu-text fw-bold">Arsip UB Apps</span>
        </a>

    </div>
    <div class="menu-inner-shadow"></div>
        @include('main.sidebar.menu')
</aside>
