<div class="dropdown-shortcuts-list scrollable-container">
    <div class="row row-bordered overflow-visible g-0">
        <div class="dropdown-shortcuts-item col">
            <span class="dropdown-shortcuts-icon rounded-circle mb-2">
                <i class="ti ti-calendar fs-4"></i>
            </span>
            <a href="app-calendar.html" class="stretched-link">Calendar</a>
            <small class="text-muted mb-0">Appointments</small>
        </div>
        <div class="dropdown-shortcuts-item col">
            <span class="dropdown-shortcuts-icon rounded-circle mb-2">
                <i class="ti ti-file-invoice fs-4"></i>
            </span>
            <a href="app-invoice-list.html" class="stretched-link">Invoice App</a>
            <small class="text-muted mb-0">Manage Accounts</small>
        </div>
    </div>
    <div class="row row-bordered overflow-visible g-0">
        <div class="dropdown-shortcuts-item col">
            <span class="dropdown-shortcuts-icon rounded-circle mb-2">
                <i class="ti ti-users fs-4"></i>
            </span>
            <a href="app-user-list.html" class="stretched-link">User App</a>
            <small class="text-muted mb-0">Manage Users</small>
        </div>
        <div class="dropdown-shortcuts-item col">
            <span class="dropdown-shortcuts-icon rounded-circle mb-2">
                <i class="ti ti-lock fs-4"></i>
            </span>
            <a href="app-access-roles.html" class="stretched-link">Role
                Management</a>
            <small class="text-muted mb-0">Permission</small>
        </div>
    </div>
    <div class="row row-bordered overflow-visible g-0">
        <div class="dropdown-shortcuts-item col">
            <span class="dropdown-shortcuts-icon rounded-circle mb-2">
                <i class="ti ti-chart-bar fs-4"></i>
            </span>
            <a href="index.html" class="stretched-link">Dashboard</a>
            <small class="text-muted mb-0">User Profile</small>
        </div>
        <div class="dropdown-shortcuts-item col">
            <span class="dropdown-shortcuts-icon rounded-circle mb-2">
                <i class="ti ti-settings fs-4"></i>
            </span>
            <a href="pages-account-settings-account.html" class="stretched-link">Setting</a>
            <small class="text-muted mb-0">Account Settings</small>
        </div>
    </div>
    <div class="row row-bordered overflow-visible g-0">
        <div class="dropdown-shortcuts-item col">
            <span class="dropdown-shortcuts-icon rounded-circle mb-2">
                <i class="ti ti-help fs-4"></i>
            </span>
            <a href="pages-help-center-landing.html" class="stretched-link">Help
                Center</a>
            <small class="text-muted mb-0">FAQs & Articles</small>
        </div>
        <div class="dropdown-shortcuts-item col">
            <span class="dropdown-shortcuts-icon rounded-circle mb-2">
                <i class="ti ti-square fs-4"></i>
            </span>
            <a href="modal-examples.html" class="stretched-link">Modals</a>
            <small class="text-muted mb-0">Useful Popups</small>
        </div>
    </div>
</div>
