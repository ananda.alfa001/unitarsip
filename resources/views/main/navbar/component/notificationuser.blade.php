<ul class="dropdown-menu dropdown-menu-end">
    <li>
        <a class="dropdown-item" href="pages-account-settings-account.html">
            <div class="d-flex">
                <div class="flex-shrink-0 me-3">
                    <div class="avatar avatar-online">
                        <img src="{{ asset('/assets/img/avatars/brone.png') }}" alt class="h-auto rounded-circle" />
                    </div>
                </div>
                <div class="flex-grow-1">
                    <span class="fw-semibold d-block">{{ Auth::user()->name }}</span>
                    <small class="text-muted">{{ Auth::user()->email }}</small>
                </div>
            </div>
        </a>
    </li>
    <li>
        <div class="dropdown-divider"></div>
    </li>
    <li>
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </a>
    </li>
</ul>
