<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="light-style layout-navbar-fixed layout-menu-fixed"
    dir="ltr" data-theme="theme-default" data-assets-path="''../../assets/" data-template="vertical-menu-template">
@include('main.components.license.license')
<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <title>Dashboard - Arsip UB Apps | Universitas Brawijaya</title>
    <meta name="description" content="" />
    <link rel="icon" type="image/x-icon" href="{{ asset('assets/home/img/logo/logo_ub.png') }}" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet" />
    {{-- Main Style --}}
    @include('main.components.style.style')
</head>
<body>
    @include('sweetalert::alert')
    <div id="loader">
        {{-- @include('main.loader.loader') --}}
    </div>
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">
            {{-- Sidebar --}}
            @include('main.sidebar.sidebar')
            <div class="layout-page">
                {{-- Navbar --}}
                @include('main.navbar.navbar')

                <div class="content-wrapper">
                    <div class="container-xxl flex-grow-1 container-p-y">
                        <div class="row">
                            {{-- Main Page --}}
                            @yield('content')
                            {{-- End OF Main Page --}}
                        </div>
                    </div>
                    {{-- Footer --}}
                    @include('main.footer.footer')
                </div>
            </div>
        </div>
        <div class="layout-overlay layout-menu-toggle"></div>
        <div class="drag-target"></div>
    </div>
    {{-- Main Javasript --}}
    @include('main.components.js.js')
</body>

</html>
