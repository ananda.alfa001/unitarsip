<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="light-style layout-navbar-fixed layout-menu-fixed"
    dir="ltr" data-theme="theme-default" data-assets-path="''../../assets/" data-template="vertical-menu-template">
@include('auth.components.license.license')
<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <title>Dashboard - Arsip UB Apps | Universitas Brawijaya</title>
    <meta name="description" content="" />
    <link rel="icon" type="image/x-icon" href="{{ asset('assets/home/img/logo/logo_ub.png') }}" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet" />
    {{-- Main Style --}}
    @include('auth.components.style.style')
</head>
<body>
    @include('sweetalert::alert')
        {{-- Main Page --}}
        @yield('content')
        {{-- End OF Main Page --}}
    {{-- Main Javasript --}}
    @include('auth.components.js.js')
</body>

</html>
