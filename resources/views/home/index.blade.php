@extends('home.main.index')
@section('content')
<!-- ======= Hero Section ======= -->
<section id="hero" class="hero">
    <div class="container position-relative">
        <div class="row gy-5" data-aos="fade-in">
            <div
                class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center text-center text-lg-start">
                <h5 style="color:#f8f8f8;">Selamat Datang di Portal</h5>
                <h2><span style="color: #ffc107;">UNIT KEARSIPAN I</span>
                    <br>
                    <span style="font-size: smaller;">UNIVERSITAS BRAWIJAYA</span>
                </h2>
                <div class="d-flex justify-content-center justify-content-lg-start">
                    <a href="https://youtube.com/watch?v=hLK41oLH8kw"
                        class="glightbox btn-watch-video d-flex align-items-center"><i
                            class="bi bi-play-circle"></i><span>Profil Brawijaya</span></a>
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2">
                <img src="{{ asset('assets/home/img/galery/brone.png') }}" class="img-fluid rounded-image"
                    alt="" data-aos="zoom-out" data-aos-delay="100">
            </div>
        </div>
    </div>
    <div class="icon-boxes position-relative">
        <div class="container position-relative">
            <div class="row gy-4 mt-5">

                <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon-box">
                        <div class="icon"><i class="bi bi-bank"></i></div>
                        <h4 class="title"><a href="{{ route('home.sejarah') }}" class="stretched-link">Sejarah Arsip Universitas
                                Brawijaya</a></h4>
                    </div>
                </div>
                <!--End Icon Box -->

                <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="200">
                    <div class="icon-box">
                        <div class="icon"><i class="bi bi-search-heart"></i></div>
                        <h4 class="title"><a href="" class="stretched-link">Pencarian Arsip Brawijaya</a>
                        </h4>
                    </div>
                </div>
                <!--End Icon Box -->

                <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="300">
                    <div class="icon-box">
                        <div class="icon"><i class="bi bi-collection"></i></div>
                        <h5 class="title"><a href="" class="stretched-link">Donor Arsip Brawijaya</a>
                        </h5>
                    </div>
                </div>
                <!--End Icon Box -->

                <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="500">
                    <div class="icon-box">
                        <div class="icon"><i class="bi bi-journal-bookmark"></i></div>
                        <h4 class="title"><a href="" class="stretched-link">Majalah Arsip Brawijaya</a>
                        </h4>
                    </div>
                </div>
                <!--End Icon Box -->

            </div>
        </div>
    </div>
    </div>
</section>
<!-- End Hero Section -->
<main id="main">
    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
        <div class="container" data-aos="fade-up">

            <div class="section-header">
                <h2>Profil Kearsipan</h2>
                <p>Sejalan dengan upaya penjaminan mutu secara berkelanjutan dan upaya mempertahankan akreditasi
                    nasional maupun internasional, Universitas Brawijaya memerlukan sistem pengendalian dokumen dan
                    rekaman yang baik.</p>
            </div>

            <div class="row gy-4">
                <div class="content col-lg-6">
                    <h3>“Archivum est Potentia” artinya arsip adalah kekuatan</h3>
                    <img src="{{ asset('assets/home/img/galery/profile.JPG') }}" class="img-fluid rounded-4 mb-4"
                        alt="">
                    <p>Tujuan bersama kami yaitu:</p>
                    <ul>
                        <li><i class="bi bi-check-circle-fill"></i>Membina seluruh unit kerja, tenaga kearsipan
                            fungsional dan non fungsional dalam mengelola arsip dinamis.</li>
                        <li><i class="bi bi-check-circle-fill"></i>Mengelola arsip statis yang diserahkan oleh unit
                            kerja yang ada di lingkungan UB</li>
                        <li><i class="bi bi-check-circle-fill"></i>Mengelola sistem informasi kearsipan di
                            lingkungan UB</li>
                    </ul>
                    <p>Maklumat pelayanan UK UB sesuai dengan maklumat pelayanan UB, yaitu :<br><br>
                        <span class="fst-italic"> “Dengan ini, Kami menyatakan sanggup menyelenggarakan pelayanan
                            sesuai standar pelayanan yang telah ditetapkan dan apabila tidak menepati janji ini,
                            kami siap menerima sanksi sesuai peraturan perundang-undangan yang berlaku”.</span>
                    </p>
                </div>
                <div class="col-lg-6">
                    <div class="content ps-0 ps-lg-5">
                        <p class="fst-italic">
                            Visi Kearsipan I Universitas Brawijaya yaitu menjadi Lembaga Kearsipan Perguruan Tinggi
                            yang menjadi Rujukan Nasional.
                        </p>
                        <ul>
                            <li><i class="bi bi-check-circle-fill"></i> Mengembangkan pengelolaan kearsipan yang
                                berkualitas (tertata, tersimpan, dan terdaftar) terintegrasi, berdayaguna, dan
                                bernilaiguna.</li>
                            <li><i class="bi bi-check-circle-fill"></i> Memasyarakatkan pentingnya arsip bagi
                                pelanggan di lingkungan internal dan eksternal Universitas Brawijaya.</li>
                            <div class="position-relative mt-4">
                                <img src="{{ asset('assets/home/img/galery/profile2.jpg') }}"
                                    class="img-fluid rounded-4" alt="">
                                {{-- <a href="" class="glightbox play-btn"></a> --}}
                            </div>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End About Us Section -->
    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio sections-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-header">
                <h2>Kearsipan Update</h2>
                <p>Informasi seputar Arsiparis Universitas Brawijaya</p>
            </div>
            <div class="portfolio-isotope" data-portfolio-filter="*" data-portfolio-layout="masonry"
                data-portfolio-sort="original-order" data-aos="fade-up" data-aos-delay="100">
                <div>
                    <ul class="portfolio-flters">
                        <li data-filter="*" class="filter-active">All</li>
                        @empty($kategori)
                        @else
                            @foreach ($kategori as $kategorifilter)
                                @if ($kategorifilter->name != null)
                                    <li data-filter=".filter-{{ $kategorifilter->name }}">{{ $kategorifilter->name }}
                                    </li>
                                @endif
                            @endforeach
                        @endempty
                    </ul>
                </div>
                <div class="row gy-4 portfolio-container">
                    @empty($berita)
                    @else
                        @foreach ($berita as $key => $beritaShow)
                            <div class="col-xl-4 col-md-6 portfolio-item filter-{{ $beritaShow->kategori->name }}">
                                <div class="portfolio-wrap">
                                    <a href="{{ asset($beritaShow->thumbnail) }}"
                                        data-gallery="portfolio-gallery-app" class="glightbox"><img
                                            src="{{ asset($beritaShow->thumbnail) }}" class="img-fluid"
                                            alt=""></a>
                                    <div class="portfolio-info">
                                        <small class="fw-semibold mb-3"
                                            style="color: #d1d1d1">{{ $beritaShow->kategori?->name }}</small>
                                        <h4><a href="{{ route('home.show', ['home' => base64_encode($beritaShow->id)]) }}"
                                                title="...">{{ $beritaShow->judul }}</a></h4>
                                        <p><a href="{{ route('home.show', ['home' => base64_encode($beritaShow->id)]) }}">Baca Berita Selengkapnya ...</a></p>
                                        <small class="mb-3"
                                            style="color: #d1d1d1;">{{ $beritaShow->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY') }}</small>
                                    </div>
                                </div>
                            </div><!-- End Portfolio Item -->
                        @endforeach
                    @endempty
                </div><!-- End Portfolio Container -->
            </div>
        </div>
    </section><!-- End Portfolio Section -->
    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients">
        <div class="container" data-aos="zoom-out">

            <div class="clients-slider swiper">
                <div class="swiper-wrapper align-items-center">
                    <div class="swiper-slide"><img src="{{ asset('assets/home/img/logo/part/logo_anri.jpg') }}"
                            class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('assets/home/img/logo/logo_ub.png') }}"
                            class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('assets/home/img/logo/part/logo_papti.png') }}"
                            class="img-fluid" alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('assets/home/img/logo/part/logo_FKMSA.png') }}"
                            class="img-fluid" alt=""></div>
                </div>
            </div>

        </div>
    </section><!-- End Clients Section -->
    <!-- ======= Stats Counter Section ======= -->
    <section id="stats-counter" class="stats-counter">
        <div class="container" data-aos="fade-up">

            <div class="row gy-4 align-items-center">

                <div class="col-lg-6">
                    <img src="assets/home/img/stats-img.svg" alt="" class="img-fluid">
                </div>

                <div class="col-lg-6">

                    <div class="stats-item d-flex align-items-center">
                        <span data-purecounter-start="0" data-purecounter-end="14" data-purecounter-duration="1"
                            class="purecounter"></span>
                        <p><strong>Biografi Rektor</strong> dari masa ke masa</p>
                    </div><!-- End Stats Item -->

                    <div class="stats-item d-flex align-items-center">
                        <span data-purecounter-start="0" data-purecounter-end="18" data-purecounter-duration="1"
                            class="purecounter"></span>
                        <p><strong>Sejarah</strong> pendirian fakultas Universitas Brawijaya</p>
                    </div><!-- End Stats Item -->

                    <div class="stats-item d-flex align-items-center">
                        <span data-purecounter-start="0" data-purecounter-end="1034"
                            data-purecounter-duration="1" class="purecounter"></span>
                        <p><strong>Surat Keputusan</strong> Rektorat Universitas Brawijaya</p>
                    </div><!-- End Stats Item -->

                </div>

            </div>

        </div>
    </section><!-- End Stats Counter Section -->

    <!-- ======= Our Services Section ======= -->
    <section id="services" class="services sections-bg">
        <div class="container" data-aos="fade-up">

            <div class="section-header">
                <h2>Layanan Kearsipan</h2>
                <p>Pelayanan kearsipan yang dilakukan oleh Unit Kearsipan I Universitas Brawijaya</p>
            </div>

            <div class="row gy-4" data-aos="fade-up" data-aos-delay="100">

                <div class="col-lg-4 col-md-6">
                    <div class="service-item  position-relative">
                        <div class="icon">
                            <i class="bi bi-person-workspace"></i>
                        </div>
                        <h3>Pembinaan Kearsipan</h3>
                        <p>Peningkatan kearsipan merupakan salah satu langkah yang dilakukan untuk meningkatkan mutu
                            pengelolaan arsip di lingkungan Universitas Brawijaya (UB). Hal ini bertujuan untuk
                            meningkatkan pemahaman dan kesadaran unit pengolah tentang pentingnya peran arsip dalam
                            menjalankan administrasi perguruan tinggi dan melaksanakan Tri Dharma UB.</p>
                        {{-- <a href="#" class="readmore stretched-link">Read more <i class="bi bi-arrow-right"></i></a> --}}
                    </div>
                </div><!-- End Service Item -->

                <div class="col-lg-4 col-md-6">
                    <div class="service-item position-relative">
                        <div class="icon">
                            <i class="bi bi-layer-forward"></i>
                        </div>
                        <h3>Peminjaman Arsip</h3>
                        <p>Peminjaman arsip adalah proses dimana arsip dikeluarkan dari tempat penyimpanannya, baik
                            oleh unit kerja yang sama maupun oleh unit kerja lain di dalam organisasi. Proses
                            peminjaman ini melibatkan pencatatan yang dilakukan oleh petugas arsip menggunakan
                            formulir pinjam arsip (out-slip).</p>
                        {{-- <a href="#" class="readmore stretched-link">Read more <i class="bi bi-arrow-right"></i></a> --}}
                    </div>
                </div><!-- End Service Item -->

                <div class="col-lg-4 col-md-6">
                    <div class="service-item position-relative">
                        <div class="icon">
                            <i class="bi bi-pc-display"></i>
                        </div>
                        <h3>Alih Media dan Digitalisasi</h3>
                        <p>
                            Alih media merupakan proses transformasi arsip dari format fisik atau tercetak menjadi
                            format digital atau elektronik. Proses ini melibatkan pengubahan arsip yang semula
                            berbentuk fisik menjadi arsip dalam bentuk elektronik atau pengolahan arsip yang sejak
                            awal sudah berbentuk digital.</p>
                        {{-- <a href="#" class="readmore stretched-link">Read more <i class="bi bi-arrow-right"></i></a> --}}
                    </div>
                </div><!-- End Service Item -->

                <div class="col-lg-4 col-md-6">
                    <div class="service-item position-relative">
                        <div class="icon">
                            <i class="bi bi-bounding-box-circles"></i>
                        </div>
                        <h3>Akuisisi Arsip</h3>
                        <p>Akuisisi Arsip Statis merupakan langkah penambahan koleksi arsip statis ke dalam Kantor
                            Arsip UB. Proses ini melibatkan penyerahan arsip statis dari unit pengolah kepada
                            Kantor Arsip, yang melalui tahapan pendokumentasian yang terstruktur, seperti penilaian,
                            penataan, dan pembuatan Daftar Pertelaan Arsip (DPA).</p>
                        {{-- <a href="#" class="readmore stretched-link">Read more <i class="bi bi-arrow-right"></i></a> --}}
                    </div>
                </div><!-- End Service Item -->

                <div class="col-lg-4 col-md-6">
                    <div class="service-item position-relative">
                        <div class="icon">
                            <i class="bi bi-pc-display"></i>
                        </div>
                        <h3>Pemusnahan Arsip</h3>
                        <p>Pemusnahan arsip adalah proses di mana arsip secara fisik dihancurkan dan identitas yang
                            terkait dengan arsip tersebut sepenuhnya dihilangkan. Tindakan ini dilakukan secara
                            menyeluruh sehingga tidak ada informasi yang dapat dikenali lagi baik dari segi isi
                            maupun bentuk arsip tersebut.</p>
                        {{-- <a href="#" class="readmore stretched-link">Read more <i class="bi bi-arrow-right"></i></a> --}}
                    </div>
                </div><!-- End Service Item -->
                <div class="col-lg-4 col-md-6">
                    <div class="service-item position-relative">
                        <div class="icon">
                            <i class="bi bi-journal-plus"></i>
                        </div>
                        <h3>Riset dan Magang</h3>
                        <p>Unit Arsip I UB menunjukkan komitmennya dalam pengabdian kepada masyarakat dengan
                            memberikan kesempatan kepada siswa, mahasiswa, masyarakat umum, dan instansi di luar UB
                            untuk berpartisipasi dalam kegiatan berbagi pengetahuan dan pengembangan kearsipan. Hal
                            ini dilakukan melalui kegiatan Studi Banding, Penelitian/Riset, dan Magang, di mana
                            mereka dapat belajar dan berkolaborasi bersama dalam bidang kearsipan.</p>
                        {{-- <a href="#" class="readmore stretched-link">Read more <i class="bi bi-arrow-right"></i></a> --}}
                    </div>
                </div><!-- End Service Item -->
            </div>
        </div>
    </section><!-- End Our Services Section -->
    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
        <div class="container" data-aos="fade-up">

            <div class="section-header">
                <h2>Quotes Kearsipan</h2>
                <p>Tiada kata tanpa arti, Tiada data tanpa dokumentasi</p>
            </div>
            <div class="slides-3 swiper" data-aos="fade-up" data-aos-delay="100">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <div class="d-flex align-items-center">
                                    <img src="{{ asset('assets/home/img/quotes/1.jpeg') }}"
                                        class="testimonial-img flex-shrink-0" alt="">
                                    <div>
                                        <h3>Ricardo J. Alfaro</h3>
                                        <h4>Presiden Panama 1931-1937</h4>
                                    </div>
                                </div>
                                <p>
                                    <i class="bi bi-quote quote-icon-left"></i>
                                    Pemerintah tanpa arsip seperti tentara tanpa senjata, dokter tanpa obat, petani
                                    tanpa benih, tukang tanpa alat. Arsip berikut adalah saksi bisu, tak
                                    terpisahkan, handal dan abadi, yang membuktikan keberhasilan, kegagalan,
                                    pertumbuhan dan kejayaan bangsa
                                    <i class="bi bi-quote quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <div class="d-flex align-items-center">
                                    <img src="{{ asset('assets/home/img/quotes/2.jpg') }}"
                                        class="testimonial-img flex-shrink-0" alt="">
                                    <div>
                                        <h3>Ralph Waldo Emerson</h3>
                                        <h4>Penyair dan Filsuf dari Amerika Serikat 1803-1882</h4>
                                    </div>
                                </div>
                                <p>
                                    <i class="bi bi-quote quote-icon-left"></i>
                                    Language is the archives of history
                                    <i class="bi bi-quote quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <div class="d-flex align-items-center">
                                    <img src="{{ asset('assets/home/img/quotes/3.jpg') }}"
                                        class="testimonial-img flex-shrink-0" alt="">
                                    <div>
                                        <h3>Adlei E. Stevenson II</h3>
                                        <h4>Politikus Amerika Serikat</h4>
                                    </div>
                                </div>
                                <p>
                                    <i class="bi bi-quote quote-icon-left"></i>
                                    The university is the archive of the Western mind, it's the keeper of the
                                    Western culture, ... the guardian of our heritage, the teacher of our teachers,
                                    ... the dwelling place of the free mind
                                    <i class="bi bi-quote quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <div class="d-flex align-items-center">
                                    <img src="{{ asset('assets/home/img/quotes/4.jpeg') }}"
                                        class="testimonial-img flex-shrink-0" alt="">
                                    <div>
                                        <h3>Dr.(H.C.) Drs. H. Muhammad Jusuf Kalla </h3>
                                        <h4>Wakil Presiden Republik Indonesia ke-10</h4>
                                    </div>
                                </div>
                                <p>
                                    <i class="bi bi-quote quote-icon-left"></i>
                                    Pekerjaan yang baik tanpa perencanaan hanya akan jadi sulit. Perencanaan yang
                                    baik tanpa pelaksanaan hanya akan jadi arsip
                                    <i class="bi bi-quote quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="testimonial-wrap">
                            <div class="testimonial-item">
                                <div class="d-flex align-items-center">
                                    <img src="{{ asset('assets/home/img/quotes/5.jpeg') }}"
                                        class="testimonial-img flex-shrink-0" alt="">
                                    <div>
                                        <h3>Sir Arthur Doughty</h3>
                                        <h4>Pengarsip Dominion dan Penjaga Catatan Publik</h4>
                                    </div>
                                </div>
                                <p>
                                    <i class="bi bi-quote quote-icon-left"></i>
                                    Dari semua aset negara yang ada, arsip adalah aset yang paling berharga. Ia
                                    merupakan warisan nasional dari generasi ke genrasi yang perlu dipelihara dan
                                    dilestarikan. Tingkat keberadaban suatu bangsa dapat dilihat dari pemeliharaan
                                    dan pelestarian terhadap arsipnya
                                    <i class="bi bi-quote quote-icon-right"></i>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section><!-- End Testimonials Section -->
</main><!-- End #main -->
@endsection
