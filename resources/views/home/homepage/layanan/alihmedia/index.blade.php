@extends('home.main.index')
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="page-header d-flex align-items-center"
                style="position: relative; width: 100%; height: 400px; background-image: url('{{ asset('assets/home/img/galery/hero.jpg') }}'); background-size: cover; background-position: center;">
                <div
                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5);">
                </div>
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Kearsipan Universitas Brawijaya</h2>
                            <p>Sejalan dengan upaya penjaminan mutu secara berkelanjutan dan upaya mempertahankan akreditasi
                                nasional maupun internasional, Universitas Brawijaya memerlukan sistem pengendalian dokumen
                                dan rekaman yang baik.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="/">Home</a></li>
                        <li>Pelayanan Alih Media dan Digitalisasi</li>
                    </ol>
                </div>
            </nav>
        </div>
        <!-- ======= Blog Details Section ======= -->
        <section id="blog" class="blog">
            <div class="container" data-aos="fade-up">
                <div class="row g-5">
                    <div class="col-lg-8">
                        <article class="blog-details">
                            <div class="post-img">
                                <img src="{{ asset('assets/home/img/galery/layanan/alihmedia/alihmedia.jpeg') }}"
                                    alt="" class="img-fluid" style="width: 100%; !important">
                            </div>
                            <h2 class="title"> Pelayanan Alih Media dan Digitalisasi Arsip Universitas Brawijaya </h2>
                            <div class="meta-top">
                                <ul>
                                    <li class="d-flex align-items-center"><i class="bi bi-person"></i>Kearsipan Brawijaya
                                    </li>
                                    <li class="d-flex align-items-center"><i class="bi bi-clock"></i><time datetime="">
                                            10:00 WIB</time></li>
                                    {{-- <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> total Pembaca</a> --}}
                                    </li>
                                </ul>
                            </div>
                            <div class="content">
                                <h3>Alih Media & Digitalisasi</h3>

                                <p>
                                    Alih media arsip dilakukan dalam rangka penggunaan dan pemeliharaan arsip aktif dan
                                    arsip inaktif pada unit pengolah di lingkungan Universitas Brawijaya. Tujuan utama
                                    dilakukan alih media adalah agar pelestarian arsip Universitas Brawijaya terjamin
                                    autentisitas, disamping itu tujuan lainnya adalah dengan teknologi alih media yang
                                    semakin canggih, akses terhadap arsip yang telah dialih mediakan dapat mudah, cepat dan
                                    tepat apabila dilakukan penemuan kembali (retrieval) arsip Universitas Brawijaya. Alih
                                    media adalah kegiatan digitalisasi arsip dari arsip tercetak yang dikonversi menjadi
                                    arsip elektronik atau arsip yang memang “lahir” dalam bentuk digital.
                                </p>

                                <h3>Tujuan</h3>
                                <ul>
                                    <li>Agar arsip atau rekaman informasi dapat diakses melalui system computer (online,
                                        offline) kapanpun dan dimanapun.</li>
                                    <li>Salah satu strategi pelestarian arsip masa kini, agar dapat selalu mengikuti
                                        perkembangan teknologi.</li>
                                    <li>Agar arsip dapat dipelihara dan dijaga di lokal repositori atau cloud storage.</li>
                                </ul>
                                <br>
                                <br>
                            </div>
                            <div class="meta-bottom">
                                <i class="bi bi-folder"></i>
                                <ul class="cats">
                                    <li>Info Kearsipan</li>
                                </ul>

                                <i class="bi bi-tags"></i>
                                <ul class="tags">
                                    <li>Informasi</li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4">
                        <div class="sidebar">
                            <div class="sidebar-item categories">
                                <h3 class="sidebar-title">Informasi lainnya</h3>
                                <ul class="mt-3">
                                    <li>informasi <span>informasi</span></li>
                                </ul>
                            </div><!-- End sidebar categories-->

                            <div class="sidebar-item recent-posts">
                                <h3 class="sidebar-title">Publikasi Berita</h3>
                                <div class="mt-3">
                                    {{-- @foreach ($beritaUpdate as $key => $beritaUpdate)
                                        <div class="post-item mt-3">
                                            <img src="{{asset($beritaUpdate->thumbnail)}}" alt="">
                                            <div>
                                                <h4><a href="{{ route('home.show', ['home' => base64_encode($beritaUpdate->id)]) }}">{{$beritaUpdate->judul}}</a></h4>
                                                <time datetime="{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}">{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}</time>
                                            </div>
                                        </div>
                                    @endforeach --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
