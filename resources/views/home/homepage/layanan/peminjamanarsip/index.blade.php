@extends('home.main.index')
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="page-header d-flex align-items-center"
                style="position: relative; width: 100%; height: 400px; background-image: url('{{ asset('assets/home/img/galery/hero.jpg') }}'); background-size: cover; background-position: center;">
                <div
                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5);">
                </div>
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Kearsipan Universitas Brawijaya</h2>
                            <p>Sejalan dengan upaya penjaminan mutu secara berkelanjutan dan upaya mempertahankan akreditasi
                                nasional maupun internasional, Universitas Brawijaya memerlukan sistem pengendalian dokumen
                                dan rekaman yang baik.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="/">Home</a></li>
                        <li>Pelayanan Peminjaman Arsip</li>
                    </ol>
                </div>
            </nav>
        </div>
        <!-- ======= Blog Details Section ======= -->
        <section id="blog" class="blog">
            <div class="container" data-aos="fade-up">
                <div class="row g-5">
                    <div class="col-lg-8">
                        <article class="blog-details">
                            <div class="post-img">
                                <img src="{{ asset('assets/home/img/galery/logo/arsipub.png') }}"
                                    alt="" class="img-fluid" style="width: 100%; !important">
                            </div>
                            <h2 class="title"> Pelayanan Peminjaman Arsip Universitas Brawijaya </h2>
                            <div class="meta-top">
                                <ul>
                                    <li class="d-flex align-items-center"><i class="bi bi-person"></i>Kearsipan Brawijaya
                                    </li>
                                    <li class="d-flex align-items-center"><i class="bi bi-clock"></i><time datetime="">
                                            10:00 WIB</time></li>
                                    {{-- <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> total Pembaca</a> --}}
                                    </li>
                                </ul>
                            </div>
                            <div class="content">
                                <h3>A. Peminjaman Arsip</h3>
                                <p>
                                    Peminjaman arsip adalah keluarnya arsip dari file, karena dipinjam baik oleh unit kerja
                                    sendiri maupun dari unit kerja lain di lingkungan Universitas Brawijaya. Peminjaman
                                    dilakukan melalui pencatatan oleh petugas arsip Unit Kearsipan I Universitas Brawijaya
                                    dengan cara menggunakan formulir pinjam arsip (out-slip).
                                </p>

                                <h3>B. Dasar Hukum</h3>
                                <ul>
                                    <li>Undang – Undang Nomor 43 Tahun 2009 Tentang Kearsipan;</li>
                                    <li>Peraturan Pemerintah Nomor 28 Tahun 2012 Tentang Pelaksanaan Undang - undang Nomor
                                        43 Tahun 2009 tentang Kearsipan;</li>
                                    <li>Peraturan Kepala ANRI Nomor 27 Tahun 2011 Tentang Pedoman Penyusunan Sarana Bantu
                                        Penemuan Kembali Arsip Statis;</li>
                                    <li>Peraturan ANRI Nomor 14 Tahun 2018 Tentang Standar Deskripsi Arsip Statis;</li>
                                </ul>

                                <h3>C. Prinsip</h3>
                                <p><strong>Asas/Prinsip Asal Usul (Provenance)</strong></p>
                                <p>
                                    Menjaga arsip tetap terkelola dalam satu kesatuan pencipta arsip, tidak dicampur dengan
                                    arsip yang berasal dari pencipta arsip lain, sehingga arsip dapat melekat pada konteks
                                    penciptanya.
                                </p>

                                <p><strong>Asas/Prinsip Aturan Asli (Original Order)</strong></p>
                                <p>
                                    Menjaga arsip tetap ditata sesuai dengan pengaturan aslinya atau sesuai dengan peraturan
                                    ketika arsip masih digunakan untuk pelaksanaan kegiatan pencipta arsip, dimaksudkan
                                    untuk menjaga keutuhan dan realibilitas arsip.
                                </p>
                                <br>
                                <br>
                            </div>
                            <div class="meta-bottom">
                                <i class="bi bi-folder"></i>
                                <ul class="cats">
                                    <li>Info Kearsipan</li>
                                </ul>

                                <i class="bi bi-tags"></i>
                                <ul class="tags">
                                    <li>Informasi</li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4">
                        <div class="sidebar">
                            <div class="sidebar-item categories">
                                <h3 class="sidebar-title">Informasi lainnya</h3>
                                <ul class="mt-3">
                                    <li>informasi <span>informasi</span></li>
                                </ul>
                            </div><!-- End sidebar categories-->

                            <div class="sidebar-item recent-posts">
                                <h3 class="sidebar-title">Publikasi Berita</h3>
                                <div class="mt-3">
                                    {{-- @foreach ($beritaUpdate as $key => $beritaUpdate)
                                        <div class="post-item mt-3">
                                            <img src="{{asset($beritaUpdate->thumbnail)}}" alt="">
                                            <div>
                                                <h4><a href="{{ route('home.show', ['home' => base64_encode($beritaUpdate->id)]) }}">{{$beritaUpdate->judul}}</a></h4>
                                                <time datetime="{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}">{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}</time>
                                            </div>
                                        </div>
                                    @endforeach --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
