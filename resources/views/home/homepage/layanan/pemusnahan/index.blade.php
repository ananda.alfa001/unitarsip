@extends('home.main.index')
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="page-header d-flex align-items-center"
                style="position: relative; width: 100%; height: 400px; background-image: url('{{ asset('assets/home/img/galery/hero.jpg') }}'); background-size: cover; background-position: center;">
                <div
                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5);">
                </div>
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Kearsipan Universitas Brawijaya</h2>
                            <p>Sejalan dengan upaya penjaminan mutu secara berkelanjutan dan upaya mempertahankan akreditasi
                                nasional maupun internasional, Universitas Brawijaya memerlukan sistem pengendalian dokumen
                                dan rekaman yang baik.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="/">Home</a></li>
                        <li>Pemusnahan Arsip</li>
                    </ol>
                </div>
            </nav>
        </div>
        <!-- ======= Blog Details Section ======= -->
        <section id="blog" class="blog">
            <div class="container" data-aos="fade-up">
                <div class="row g-5">
                    <div class="col-lg-8">
                        <article class="blog-details">
                            <div class="post-img">
                                <img src="{{ asset('assets/home/img/galery/pemusnahan/pemusnahan.jpg') }}" alt=""
                                    class="img-fluid" style="width: 100%; !important">
                            </div>
                            <h2 class="title"> Pemusnahan Arsip Universitas Brawijaya </h2>
                            <div class="meta-top">
                                <ul>
                                    <li class="d-flex align-items-center"><i class="bi bi-person"></i>Kearsipan Brawijaya
                                    </li>
                                    <li class="d-flex align-items-center"><i class="bi bi-clock"></i><time datetime="">
                                            10:00 WIB</time></li>
                                    {{-- <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> total Pembaca</a> --}}
                                    </li>
                                </ul>
                            </div>
                            <div class="content">
                                <h3>Pemusnahan Arsip</h3>

                                <p>
                                    Pemusnahan Arsip adalah kegiatan memusnahkan arsip yang tidak mempunyai nilai kegunaan
                                    dan telah melampaui jangka waktu penyimpanan. Tujuan pemusnahan arsip antara lain adalah
                                    untuk efisiensi dan efektivitas kerja, serta penyelamatan informasi arsip itu sendiri
                                    dari pihak-pihak yang tidak berhak untuk mengetahuinya. Tentu yang menjadi pertimbangan
                                    mendasar dalam pemusnahan arsip yaitu harus memperhatikan kepentingan pencipta arsip
                                    serta kepentingan masyarakat, bangsa, dan negara.
                                </p>
                                <p>
                                    Terjadi kontradiksi di lapangan, di satu sisi arsip terus menumpuk karena tiadanya
                                    kegiatan pemusnahan. Di sisi yang lain, terjadi pemusnahan arsip tanpa mengacu prosedur
                                    yang benar. Atas dasar fakta tersebut perlu dicarikan solusi yang tepat agar pelaksanaan
                                    pemusnahan arsip dapat terlaksana efektif dengan prosedur yang benar.
                                </p>

                                <h3>Prinsip</h3>
                                <ul>
                                    <li>Pemusnahan arsip harus sesuai dengan prosedur dan peraturan perundang-undangan yang
                                        berlaku;</li>
                                    <li>Pemusnahan arsip menjadi tanggung jawab pencipta Arsip.</li>
                                    <li>Pemusnahan arsip hanya dilakukan oleh Unit Kearsipan setelah memperoleh persetujuan
                                        pimpinan pencipta arsip dan atau Kepala ANRI.</li>
                                    <li>Secara fisik pemusnahan dapat dilakukan di lingkungan Unit Kearsipan atau di tempat
                                        lain di bawah koordinasi dan tanggung jawab Unit Kearsipan Pencipta Arsip yang
                                        bersangkutan.</li>
                                    <li>Pemusnahan non arsip seperti: formulir kosong, amplop, undangan dan duplikasi
                                        sebagai hasil penyiangan dapat dilaksanakan di masing-masing Unit Pengolah.</li>
                                    <li>Pemusnahan arsip dilakukan secara total sehingga tidak dikenal lagi baik fisik
                                        maupun informasinya.</li>
                                </ul>

                                <h3>Kriteria Arsip yang dimusnahkan</h3>
                                <ul>
                                    <li>Tidak memiliki nilai guna baik nilai guna primer maupun nilai guna sekunder;</li>
                                    <li>Telah habis retensinya dan berketerangan dimusnahkan berdasarkan JRA;</li>
                                    <li>Tidak ada peraturan perundang-undangan yang melarang;</li>
                                    <li>Tidak berkaitan dengan penyelesaian proses suatu perkara.</li>
                                </ul>
                                <br>
                                <br>
                            </div>
                            <div class="meta-bottom">
                                <i class="bi bi-folder"></i>
                                <ul class="cats">
                                    <li>Info Kearsipan</li>
                                </ul>

                                <i class="bi bi-tags"></i>
                                <ul class="tags">
                                    <li>Informasi</li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4">
                        <div class="sidebar">
                            <div class="sidebar-item categories">
                                <h3 class="sidebar-title">Informasi lainnya</h3>
                                <ul class="mt-3">
                                    <li>informasi <span>informasi</span></li>
                                </ul>
                            </div><!-- End sidebar categories-->

                            <div class="sidebar-item recent-posts">
                                <h3 class="sidebar-title">Publikasi Berita</h3>
                                <div class="mt-3">
                                    {{-- @foreach ($beritaUpdate as $key => $beritaUpdate)
                                        <div class="post-item mt-3">
                                            <img src="{{asset($beritaUpdate->thumbnail)}}" alt="">
                                            <div>
                                                <h4><a href="{{ route('home.show', ['home' => base64_encode($beritaUpdate->id)]) }}">{{$beritaUpdate->judul}}</a></h4>
                                                <time datetime="{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}">{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}</time>
                                            </div>
                                        </div>
                                    @endforeach --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
