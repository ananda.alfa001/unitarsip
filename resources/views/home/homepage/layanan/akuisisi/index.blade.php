@extends('home.main.index')
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="page-header d-flex align-items-center"
                style="position: relative; width: 100%; height: 400px; background-image: url('{{ asset('assets/home/img/galery/hero.jpg') }}'); background-size: cover; background-position: center;">
                <div
                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5);">
                </div>
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Kearsipan Universitas Brawijaya</h2>
                            <p>Sejalan dengan upaya penjaminan mutu secara berkelanjutan dan upaya mempertahankan akreditasi
                                nasional maupun internasional, Universitas Brawijaya memerlukan sistem pengendalian dokumen
                                dan rekaman yang baik.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="/">Home</a></li>
                        <li>Akuisisi Arsip</li>
                    </ol>
                </div>
            </nav>
        </div>
        <!-- ======= Blog Details Section ======= -->
        <section id="blog" class="blog">
            <div class="container" data-aos="fade-up">
                <div class="row g-5">
                    <div class="col-lg-8">
                        <article class="blog-details">
                            <div class="post-img">
                                <img src="{{ asset('assets/home/img/galery/layanan/akuisisi/akuisisi.jpg') }}"
                                    alt="" class="img-fluid" style="width: 100%; !important">
                            </div>
                            <h2 class="title"> Pelayanan Akuisisi Arsip Universitas Brawijaya </h2>
                            <div class="meta-top">
                                <ul>
                                    <li class="d-flex align-items-center"><i class="bi bi-person"></i>Kearsipan Brawijaya
                                    </li>
                                    <li class="d-flex align-items-center"><i class="bi bi-clock"></i><time datetime="">
                                            10:00 WIB</time></li>
                                    {{-- <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> total Pembaca</a> --}}
                                    </li>
                                </ul>
                            </div>
                            <div class="content">
                                <h3>A. Akuisisi Arsip</h3>
                                <p>
                                    Akuisisi arsip statis adalah proses penambahan khazanah arsip statis pada lembaga
                                    kearsipan yang dilaksanakan melalui kegiatan penyerahan arsip statis dan hak
                                    pengelolaannya dari pencipta arsip kepada lembaga kearsipan. Pelaksanaan akuisisi arsip
                                    statis merupakan tindak lanjut dari kegiatan monitoring keberadaan arsip yang memiliki
                                    potensi arsip statis yang berada di lingkungannya. Monitoring dilakukan dengan cara
                                    penelusuran arsip statis di lingkungan pencipta arsip dengan tujuan untuk memudahkan
                                    pelaksanaan akuisisi arsip oleh lembaga kearsipan. Lembaga kearsipan harus melaksanakan
                                    akuisisi arsip statis sesuai dengan kaidah-kaidah kearsipan dan peraturan
                                    perundang-undangan.
                                </p>

                                <h3>B. Prinsip</h3>
                                <p>
                                    Akuisisi arsip statis dilakukan dengan cara penarikan arsip statis oleh lembaga
                                    kearsipan dari pencipta arsip, maupun serah terima arsip statis dari pencipta arsip
                                    kepada lembaga kearsipan. Arsip statis yang akan diakuisisi ke lembaga kearsipan telah
                                    ditetapkan sebagai arsip statis melalui proses penilaian berdasarkan pedoman penilaian
                                    kriteria dan jenis arsip yang memiliki nilaiguna sekunder, dan telah dinyatakan selesai
                                    masa simpan dinamisnya. Arsip statis yang diakuisisi dalam keadaan teratur dan terdaftar
                                    dengan baik sesuai dengan bentuk dan media serta mengacu pada prinsip asal usul dan
                                    aturan asli.
                                </p>
                                <p>
                                    Serah terima arsip statis dari hasil kegiatan akuisisi arsip statis wajib
                                    didokumentasikan melalui pembuatan naskah serah terima arsip, berupa berita acara serah
                                    terima arsip statis, daftar arsip statis yang diserahkan berikut riwayat arsip, dan
                                    arsipnya. Akuisisi arsip statis oleh lembaga kearsipan diikuti dengan peralihan
                                    tanggungjawab pengelolaannya.
                                </p>

                                <h3>C. Strategi Akuisisi</h3>
                                <p>
                                    Setiap arsip statis yang akan diakuisisi merupakan tanggung jawab lembaga kearsipan dan
                                    pencipta arsip. Informasi arsip statis yang diakuisisi tersebut merupakan hasil tahapan
                                    kegiatan akuisisi arsip statis mulai dari sejak pendataan, penataan, penilaian, dan
                                    penyerahan arsip statis. Kegiatan akuisisi arsip statis merupakan tahap awal dalam
                                    konteks pengelolaan arsip statis yang dilaksanakan oleh lembaga kearsipan untuk menambah
                                    khazanah arsip statis. Sebagai tahap awal maka kegiatan akuisisi arsip statis dilakukan
                                    dengan strategi akuisisi atau garis haluan akuisisi sehingga pelaksanaan akuisisi arsip
                                    statis dapat mencapai tujuan pengelolaan arsip statis. Strategi akuisisi arsip statis
                                    bertujuan untuk:
                                </p>
                                <ul>
                                    <li>Mengarahkan keseluruhan kegiatan sesuai dengan sasaran akuisisi arsip statis;</li>
                                    <li>Memberi batasan-batasan yang perlu dilakukan untuk memperoleh arsip statis;</li>
                                    <li>Mencegah terjadinya perolehan arsip yang tidak layak disimpan secara permanen;</li>
                                    <li>Mengatur proses serah terima arsip antara pihak lembaga kearsipan dengan pencipta
                                        arsip;</li>
                                    <li>Mengontrol keseluruhan penyelenggaraan kegiatan akuisisi.</li>
                                </ul>
                                <br>
                                <br>
                            </div>
                            <div class="meta-bottom">
                                <i class="bi bi-folder"></i>
                                <ul class="cats">
                                    <li>Info Kearsipan</li>
                                </ul>

                                <i class="bi bi-tags"></i>
                                <ul class="tags">
                                    <li>Informasi</li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4">
                        <div class="sidebar">
                            <div class="sidebar-item categories">
                                <h3 class="sidebar-title">Informasi lainnya</h3>
                                <ul class="mt-3">
                                    <li>informasi <span>informasi</span></li>
                                </ul>
                            </div><!-- End sidebar categories-->

                            <div class="sidebar-item recent-posts">
                                <h3 class="sidebar-title">Publikasi Berita</h3>
                                <div class="mt-3">
                                    {{-- @foreach ($beritaUpdate as $key => $beritaUpdate)
                                        <div class="post-item mt-3">
                                            <img src="{{asset($beritaUpdate->thumbnail)}}" alt="">
                                            <div>
                                                <h4><a href="{{ route('home.show', ['home' => base64_encode($beritaUpdate->id)]) }}">{{$beritaUpdate->judul}}</a></h4>
                                                <time datetime="{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}">{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}</time>
                                            </div>
                                        </div>
                                    @endforeach --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
