@extends('home.main.index')
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="page-header d-flex align-items-center"
                style="position: relative; width: 100%; height: 400px; background-image: url('{{ asset('assets/home/img/galery/hero.jpg') }}'); background-size: cover; background-position: center;">
                <div
                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5);">
                </div>
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Kearsipan Universitas Brawijaya</h2>
                            <p>Sejalan dengan upaya penjaminan mutu secara berkelanjutan dan upaya mempertahankan akreditasi
                                nasional maupun internasional, Universitas Brawijaya memerlukan sistem pengendalian dokumen
                                dan rekaman yang baik.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="/">Home</a></li>
                        <li>Pembinaan Arsip</li>
                    </ol>
                </div>
            </nav>
        </div>
        <!-- ======= Blog Details Section ======= -->
        <section id="blog" class="blog">
            <div class="container" data-aos="fade-up">
                <div class="row g-5">
                    <div class="col-lg-8">
                        <article class="blog-details">
                            <div class="post-img">
                                <img src="{{ asset('assets/home/img/galery/layanan/pembinaankearsipan/pembinaan.jpeg') }}"
                                    alt="" class="img-fluid" style="width: 100%; !important">
                            </div>
                            <h2 class="title"> Pembinaan Arsip Universitas Brawijaya </h2>
                            <div class="meta-top">
                                <ul>
                                    <li class="d-flex align-items-center"><i class="bi bi-person"></i>Kearsipan Brawijaya
                                    </li>
                                    <li class="d-flex align-items-center"><i class="bi bi-clock"></i><time datetime="">
                                            10:00 WIB</time></li>
                                    {{-- <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> total Pembaca</a> --}}
                                    </li>
                                </ul>
                            </div>
                            <div class="content">
                                <h3>A. Pembinaan Kearsipan</h3>
                                <p>
                                    Pembinaan kearsipan merupakan kegiatan pembinaan kearsipan di Lingkungan Universitas
                                    Brawijaya, dengan maksud terciptanya tata Kelola kearsipan yang sesuai regulasi dan
                                    meningkatkan kualitas layanan prima kepada masyarakat atau pengguna arsip serta
                                    penyelamatan
                                    arsip Universitas Brawijaya.
                                </p>

                                <h3>B. Tujuan</h3>
                                <ul>
                                    <li>Meningkatnya pemahaman dan kesadaran pengelola arsip di lingkungan Universitas
                                        Brawijaya
                                        tentang arti pentingnya arsip bagi kehidupan bermasyarakat, berbangsa, dan
                                        bernegara.
                                    </li>
                                    <li>Meningkatnya kemampuan melakukan pengelolaan arsip bagi Unit Kearsipan II di
                                        Lingkungan
                                        Universitas Brawijaya.</li>
                                    <li>Tersedianya kebijakan yang mendukung pengelolaan arsip di Universitas Brawijaya.
                                    </li>
                                    <li>Tersedianya sumber daya pendukung yang memenuhi standar dan kualitas dalam mendukung
                                        pengelolaan arsip pada Unit Kearsipan II di Lingkungan Universitas Brawijaya.</li>
                                </ul>

                                <h3>C. Sasaran</h3>
                                <p>
                                    Sasaran pembinaan dalam penyelenggaraan kearsipan yang dimiliki pencipta arsip, dalam
                                    kaitannya dengan pelaksanaan peran dan tanggungjawabnya dalam pelaksanaan penyusutan
                                    arsip
                                    dan pembinaan kearsipan adalah Unit Kearsipan II di Lingkungan Universitas Brawijaya.
                                </p>

                                <h3>D. Peran dan Tanggungjawab Pembinaan</h3>
                                <p>
                                    Perguruan Tinggi memiliki peran untuk melakukan pembinaan terhadap satuan kerja dan
                                    civitas
                                    akademika di lingkungan perguruan tinggi yang bersangkutan. Pembagian peran dan tanggung
                                    jawab yang jelas antar Lembaga Kearsipan diharapkan dapat menghindarkan kegiatan yang
                                    tumpang tindih dan mengakibatkan pemborosan/inefisiensi. Kesamaan obyek pembinaan dalam
                                    rumusan dalam Pasal 5 Undang-Undang Nomor 43 Tahun 2009 harus dipahami sebagai perumusan
                                    tanggungjawab secara berlapis guna mengantisipasi kekurang siapan Lembaga Kearsipan yang
                                    seharusnya memiliki tanggungjawab pembinaan.
                                </p>
                                <br>
                                <br>
                            </div>
                            <div class="meta-bottom">
                                <i class="bi bi-folder"></i>
                                <ul class="cats">
                                    <li>Info Kearsipan</li>
                                </ul>

                                <i class="bi bi-tags"></i>
                                <ul class="tags">
                                    <li>Informasi</li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4">
                        <div class="sidebar">
                            <div class="sidebar-item categories">
                                <h3 class="sidebar-title">Informasi lainnya</h3>
                                <ul class="mt-3">
                                    <li>informasi <span>informasi</span></li>
                                </ul>
                            </div><!-- End sidebar categories-->

                            <div class="sidebar-item recent-posts">
                                <h3 class="sidebar-title">Publikasi Berita</h3>
                                <div class="mt-3">
                                    {{-- @foreach ($beritaUpdate as $key => $beritaUpdate)
                                        <div class="post-item mt-3">
                                            <img src="{{asset($beritaUpdate->thumbnail)}}" alt="">
                                            <div>
                                                <h4><a href="{{ route('home.show', ['home' => base64_encode($beritaUpdate->id)]) }}">{{$beritaUpdate->judul}}</a></h4>
                                                <time datetime="{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}">{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}</time>
                                            </div>
                                        </div>
                                    @endforeach --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
