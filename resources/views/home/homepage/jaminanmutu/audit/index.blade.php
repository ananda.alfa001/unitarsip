@extends('home.main.index')
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="page-header d-flex align-items-center"
                style="position: relative; width: 100%; height: 400px; background-image: url('{{ asset('assets/home/img/galery/hero.jpg') }}'); background-size: cover; background-position: center;">
                <div
                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5);">
                </div>
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Kearsipan Universitas Brawijaya</h2>
                            <p>Sejalan dengan upaya penjaminan mutu secara berkelanjutan dan upaya mempertahankan akreditasi
                                nasional maupun internasional, Universitas Brawijaya memerlukan sistem pengendalian dokumen
                                dan rekaman yang baik.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="/">Home</a></li>
                        <li>Audit</li>
                    </ol>
                </div>
            </nav>
        </div>
        <!-- ======= Blog Details Section ======= -->
        <section id="blog" class="blog">
            <div class="container" data-aos="fade-up">
                <div class="row g-5">
                    <div class="col-lg-8">
                        <article class="blog-details">
                            <div class="post-img">
                                <img src="{{ asset('assets/home/img/galery/jaminanmutu/audit/audit.jpeg') }}" alt=""
                                    class="img-fluid" style="width: 100%; !important">
                            </div>
                            <h2 class="title"> Audit Kearsipan Universitas Brawijaya </h2>
                            <div class="meta-top">
                                <ul>
                                    <li class="d-flex align-items-center"><i class="bi bi-person"></i>Kearsipan Brawijaya
                                    </li>
                                    <li class="d-flex align-items-center"><i class="bi bi-clock"></i><time datetime="">
                                            10:00 WIB</time></li>
                                    {{-- <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> total Pembaca</a> --}}
                                    </li>
                                </ul>
                            </div>
                            <div class="content">
                                Audit Kearsipan adalah suatu proses yang sistematis dan menyeluruh dalam mengidentifikasi,
                                menganalisis, dan mengevaluasi berbagai aspek terkait manajemen dokumen dan rekaman yang
                                dilakukan secara independen, objektif, dan profesional. Audit ini dilakukan dengan merujuk
                                pada standar kearsipan yang telah ditetapkan untuk menilai kebenaran, kecermatan,
                                kredibilitas, efektivitas, efisiensi, dan keandalan penyelenggaraan kearsipan dalam suatu
                                organisasi atau lembaga.
                                <br>
                            </div>
                            <div class="content">
                                Dalam proses audit kearsipan, auditor akan melakukan penelitian mendalam terhadap
                                praktik-praktik kearsipan yang ada, termasuk pengelolaan, pengarsipan, pemeliharaan, dan
                                akses terhadap dokumen dan rekaman. Auditor juga akan mengevaluasi kepatuhan terhadap
                                kebijakan, prosedur, dan peraturan yang berlaku dalam pengelolaan arsip.
                                Selama proses audit, auditor akan mengumpulkan bukti-bukti yang relevan, seperti dokumen,
                                catatan, dan informasi lainnya, serta melakukan wawancara dengan personel terkait. Tujuan
                                dari audit kearsipan adalah untuk mengidentifikasi kelemahan atau masalah yang ada dalam
                                sistem kearsipan, serta memberikan rekomendasi perbaikan atau tindakan yang diperlukan untuk
                                meningkatkan efisiensi dan efektivitas manajemen arsip dalam mencapai tujuan organisasi atau
                                lembaga tersebut.
                                <br>
                                <br>
                            </div>
                            <div class="meta-bottom">
                                <i class="bi bi-folder"></i>
                                <ul class="cats">
                                    <li>Info Kearsipan</li>
                                </ul>

                                <i class="bi bi-tags"></i>
                                <ul class="tags">
                                    <li>Informasi</li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4">
                        <div class="sidebar">
                            <div class="sidebar-item categories">
                                <h3 class="sidebar-title">Informasi lainnya</h3>
                                <ul class="mt-3">
                                    <li>informasi <span>informasi</span></li>
                                </ul>
                            </div><!-- End sidebar categories-->

                            <div class="sidebar-item recent-posts">
                                <h3 class="sidebar-title">Publikasi Berita</h3>
                                <div class="mt-3">
                                    {{-- @foreach ($beritaUpdate as $key => $beritaUpdate)
                                        <div class="post-item mt-3">
                                            <img src="{{asset($beritaUpdate->thumbnail)}}" alt="">
                                            <div>
                                                <h4><a href="{{ route('home.show', ['home' => base64_encode($beritaUpdate->id)]) }}">{{$beritaUpdate->judul}}</a></h4>
                                                <time datetime="{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}">{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}</time>
                                            </div>
                                        </div>
                                    @endforeach --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
