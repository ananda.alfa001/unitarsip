@extends('home.main.index')
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="page-header d-flex align-items-center"
                style="position: relative; width: 100%; height: 400px; background-image: url('{{ asset('assets/home/img/galery/hero.jpg') }}'); background-size: cover; background-position: center;">
                <div
                    style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5);">
                </div>
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Kearsipan Universitas Brawijaya</h2>
                            <p>Sejalan dengan upaya penjaminan mutu secara berkelanjutan dan upaya mempertahankan akreditasi
                                nasional maupun internasional, Universitas Brawijaya memerlukan sistem pengendalian dokumen
                                dan rekaman yang baik.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="/">Home</a></li>
                        <li>Visi Misi</li>
                    </ol>
                </div>
            </nav>
        </div>
        <!-- ======= Blog Details Section ======= -->
        <section id="blog" class="blog">
            <div class="container" data-aos="fade-up">
                <div class="row g-5">
                    <div class="col-lg-8">
                        <article class="blog-details">
                            <div class="post-img">
                                <img src="{{ asset('assets/home/img/galery/visimisi/visimisi.jpg') }}" alt="" class="img-fluid"
                                    style="width: 100%; !important">
                            </div>
                            <h2 class="title"> Visi Misi Kearsipan Universitas Brawijaya </h2>
                            <div class="meta-top">
                                <ul>
                                    <li class="d-flex align-items-center"><i class="bi bi-person"></i>Kearsipan Brawijaya
                                    </li>
                                    <li class="d-flex align-items-center"><i class="bi bi-clock"></i><time datetime="">
                                            10:00 WIB</time></li>
                                    {{-- <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> total Pembaca</a> --}}
                                    </li>
                                </ul>
                            </div>
                            <div class="content">
                            <h2 class="mt-5">Visi</h2>
                            <p><i>"Menjadi Pusat Kearsipan Unggulan yang Mendukung Visi Universitas Brawijaya sebagai Perguruan
                                Tinggi Pelopor dan Pembaharu dengan Reputasi Internasional dalam Ilmu Pengetahuan dan
                                Teknologi, Terutama yang Menunjang Industri Berbasis Budaya untuk Kesejahteraan Masyarakat."
                            </i></p>

                            <h2>Misi</h2>
                            <ul>
                                <li>Mengelola dan Pelestarian Arsip: Kami akan mengelola dan melestarikan semua jenis
                                    dokumen dan arsip universitas dengan cermat dan sistematis. Ini mencakup dokumen
                                    akademik, administratif, riset, dan sejarah universitas.</li>
                                <li>Akses dan Layanan Kearsipan: Kami akan menyediakan layanan yang mudah diakses bagi semua
                                    pemangku kepentingan, baik itu mahasiswa, staf, peneliti, atau masyarakat umum. Kami
                                    akan memastikan bahwa informasi yang diperlukan dapat diakses dengan cepat dan efisien.
                                </li>
                                <li>Pengembangan Keterampilan Kearsipan: Kami akan mengembangkan keterampilan dan
                                    pengetahuan staf kearsipan kami secara terus-menerus untuk memastikan bahwa praktik
                                    kearsipan kami selalu sesuai dengan standar terbaru dalam industri.</li>
                                <li>Kolaborasi Internasional: Kami akan menjalin kemitraan dan kolaborasi dengan lembaga
                                    kearsipan internasional untuk pertukaran pengetahuan dan praktik terbaik dalam
                                    kearsipan.</li>
                                <li>Pengembangan Teknologi Kearsipan: Kami akan terus mengadopsi teknologi terkini dalam
                                    kearsipan, termasuk sistem manajemen arsip digital dan solusi keamanan data, untuk
                                    memastikan keamanan dan ketersediaan informasi.</li>
                                <li>Pendukung Industri Berbasis Budaya: Kami akan berperan dalam mendukung pengembangan
                                    industri berbasis budaya dengan melestarikan dan mempromosikan budaya lokal melalui
                                    dokumen dan arsip yang relevan.</li>
                                <li>Pengembangan Komunitas: Kami akan mengembangkan program pendidikan dan pengembangan
                                    komunitas untuk meningkatkan pemahaman masyarakat tentang kepentingan kearsipan dan
                                    sejarah universitas dalam konteks industri berbasis budaya.</li>
                                <li>Evaluasi dan Peningkatan Berkelanjutan: Kami akan secara rutin mengevaluasi praktik
                                    kearsipan kami dan mencari cara untuk terus meningkatkan efisiensi dan efektivitas kami
                                    dalam mendukung visi Universitas Brawijaya.</li>
                            </ul>

                            <p>Misi ini akan membantu Kearsipan Universitas Brawijaya untuk menjadi mitra yang kuat dalam
                                mencapai visi universitas untuk menjadi pelopor dalam ilmu pengetahuan dan teknologi, serta
                                berkontribusi pada kesejahteraan masyarakat melalui industri berbasis budaya.</p>
                            <br>
                            </div>
                            <div class="meta-bottom">
                                <i class="bi bi-folder"></i>
                                <ul class="cats">
                                    <li>Info Kearsipan</li>
                                </ul>

                                <i class="bi bi-tags"></i>
                                <ul class="tags">
                                    <li>Informasi</li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4">
                        <div class="sidebar">
                            <div class="sidebar-item categories">
                                <h3 class="sidebar-title">Informasi lainnya</h3>
                                <ul class="mt-3">
                                    <li>informasi <span>informasi</span></li>
                                </ul>
                            </div><!-- End sidebar categories-->

                            <div class="sidebar-item recent-posts">
                                <h3 class="sidebar-title">Publikasi Berita</h3>
                                <div class="mt-3">
                                    {{-- @foreach ($beritaUpdate as $key => $beritaUpdate)
                                        <div class="post-item mt-3">
                                            <img src="{{asset($beritaUpdate->thumbnail)}}" alt="">
                                            <div>
                                                <h4><a href="{{ route('home.show', ['home' => base64_encode($beritaUpdate->id)]) }}">{{$beritaUpdate->judul}}</a></h4>
                                                <time datetime="{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}">{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}</time>
                                            </div>
                                        </div>
                                    @endforeach --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
