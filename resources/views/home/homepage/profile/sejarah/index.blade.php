@extends('home.main.index')
@section('content')
    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="page-header d-flex align-items-center" style="position: relative; width: 100%; height: 400px; background-image: url('{{ asset('assets/home/img/galery/hero.jpg') }}'); background-size: cover; background-position: center;">
                <div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5);"></div>
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Kearsipan Universitas Brawijaya</h2>
                            <p>Sejalan dengan upaya penjaminan mutu secara berkelanjutan dan upaya mempertahankan akreditasi
                                nasional maupun internasional, Universitas Brawijaya memerlukan sistem pengendalian dokumen
                                dan rekaman yang baik.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="/">Home</a></li>
                        <li>Sejarah Universitas Brawijaya</li>
                    </ol>
                </div>
            </nav>
        </div>
        <!-- ======= Blog Details Section ======= -->
        <section id="blog" class="blog">
            <div class="container" data-aos="fade-up">
                <div class="row g-5">
                    <div class="col-lg-8">
                        <article class="blog-details">
                            <div class="post-img">
                                <img src="{{ asset('assets/home/img/galery/sejarah/sejarah.png') }}" alt="" class="img-fluid" style="width: 100%; !important">
                            </div>
                            <h2 class="title"> Sejarah Universitas Brawijaya </h2>
                            <div class="meta-top">
                                <ul>
                                    <li class="d-flex align-items-center"><i class="bi bi-person"></i>Kearsipan Brawijaya </li>
                                    <li class="d-flex align-items-center"><i class="bi bi-clock"></i><time datetime="">
                                        10:00 WIB</time></li>
                                    {{-- <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> total Pembaca</a> --}}
                                    </li>
                                </ul>
                            </div>
                            <div class="content">
                                Sejalan dengan upaya penjaminan mutu secara berkelanjutan dan upaya mempertahankan
                                akreditasi nasional maupun internasional, Universitas Brawijaya memerlukan sistem
                                pengendalian dokumen dan rekaman yang baik. Universitas Brawijaya secara rutin melakukan
                                audit internal mutu baik untuk unit kerja pelaksana akademik (UKPA) maupun unit kerja
                                pelaksana penunjang akademik (UKPPA). Audit internal mutu (AIM) unit kerja pelaksana
                                akademik (UKPPA) siklus 7 yang dilaksanakan bulan Mei Tahun 2014 menjadi tonggak awal yang
                                melandasi terbentuknya lembaga kearsipan Universitas Brawijaya. Berdasarkan hasil Audit
                                internal mutu (AIM) siklus 7 tersebut memberikan rekomendasi agar ada kebijakan untuk
                                menetapkan mekanisme pengelolaan rekaman atau arsip dan dokumen termasuk sumber daya manusia
                                dan sarana prasarananya.
                                <br>
                            </div>
                            <div class="content">
                                Pimpinan Universitas Brawijaya merealisasikan rekomendasi tersebut dengan mendirikan Unit
                                Kearsipan dan memasukkan dalam draft SOTK Universitas Brawijaya tahun 2015 pada awal tahun
                                2015. Selanjutnya pada bulan Maret 2015 Rektor menetapkan Pimpinan Unit Kearsipan yang
                                pertama dengan SK Rektor Nomor: 124 Tahun 2015 dengan Kepala Unit Kearsipan Dr. Hamidah
                                Nayati Utami, S.Sos, MSi. Dan menetapkan SK Struktur Organisasi Unit Kearsipan Nomor 136
                                Tahun 2015, tertanggal 20 Maret 2015. Unit Kearsipan Universitas Brawijaya (UK UB) adalah
                                Lembaga Kearsipan Perguruan Tinggi yang melaksanakan tugas sesuai amanah Undang-Undang No.
                                43 tahun 2009 tentang Kearsipan dan PP 12 tahun 2012 tentang pelaksanaan Undang-Undang
                                Kearsipan. Lembaga Kearsipan ini memiliki fungsi, tugas, dan tanggung jawab di bidang
                                pengelolaan arsip statis dan pembinaan kearsipan di lingkungan Kantor Pusat, Fakultas,
                                Lembaga, dan Unit yang ada di Universitas Brawijaya.
                                <br>
                                <br>
                            </div>
                            <div class="meta-bottom">
                                <i class="bi bi-folder"></i>
                                <ul class="cats">
                                    <li>Info Kearsipan</li>
                                </ul>

                                <i class="bi bi-tags"></i>
                                <ul class="tags">
                                    <li>Informasi</li>
                                </ul>
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4">
                        <div class="sidebar">
                            <div class="sidebar-item categories">
                                <h3 class="sidebar-title">Informasi lainnya</h3>
                                <ul class="mt-3">
                                    <li>informasi <span>informasi</span></li>
                                </ul>
                            </div><!-- End sidebar categories-->

                            <div class="sidebar-item recent-posts">
                                <h3 class="sidebar-title">Publikasi Berita</h3>
                                <div class="mt-3">
                                    {{-- @foreach ($beritaUpdate as $key => $beritaUpdate)
                                        <div class="post-item mt-3">
                                            <img src="{{asset($beritaUpdate->thumbnail)}}" alt="">
                                            <div>
                                                <h4><a href="{{ route('home.show', ['home' => base64_encode($beritaUpdate->id)]) }}">{{$beritaUpdate->judul}}</a></h4>
                                                <time datetime="{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}">{{$beritaUpdate->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}</time>
                                            </div>
                                        </div>
                                    @endforeach --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
