@extends('home.main.index')
@section('content')

    <main id="main">
        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
           <div class="page-header d-flex align-items-center" style="position: relative; width: 100%; height: 400px; background-image: url('{{ asset('assets/home/img/galery/hero.jpg') }}'); background-size: cover; background-position: center;">
                <div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0, 0, 0, 0.5);"></div>
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Dokumen Arsip {{ $kategori->name }}</h2>
                            <p>Dalam ruang lingkupnya yang luas, dokumen arsip universitas menciptakan jaringan informasi yang kaya, mencatat pencapaian akademis, proyek penelitian, dan perkembangan kurikulum. Melalui dokumen ini, kita dapat melacak evolusi nilai-nilai universitas, perubahan struktural, serta kontribusi signifikan terhadap ilmu pengetahuan dan masyarakat.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="/">Home</a></li>
                        <li>Dokumen Publikasi</li>
                        <li>{{ $kategori->name }}</li>
                    </ol>
                </div>
            </nav>
        </div>
        <!-- ======= Dokument Details Section ======= -->
        <section id="blog" class="blog">
            <div class="container" data-aos="fade-up">
                <div class="row g-5">
                    <div class="col-lg-8">
                        @foreach($dokumen as $dokumenShow)
                            <article class="blog-details mb-3">
                                <div class="post-img">
                                    <h2 class="title p-3">{{$dokumenShow->judul}}</h2>
                                    <hr style="margin-top: 0; margin-bottom: 0; margin-left: 15px; margin-right: 15px;">
                                    <embed class="p-3" src="{{ asset($dokumenShow->document) }}" type="application/pdf" width="100%" height="800px" />
                                </div>
                                {{$dokumenShow->desc}}
                                <div class="meta-top">
                                    <ul>
                                        {{-- <li class="d-flex align-items-center"><i class="bi bi-person"></i>{{$berita->author}}</li>
                                        <li class="d-flex align-items-center"><i class="bi bi-clock"></i><time datetime="{{$berita->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}">{{$berita->created_at->locale('id')->isoFormat('dddd, DD MMMM YYYY')}}</time></li>
                                        <li class="d-flex align-items-center"><i class="bi bi-chat-dots"></i> {{$pembaca}} Pembaca</a></li> --}}
                                    </ul>
                                </div>
                                <div class="content">

                                </div>
                                <div class="meta-bottom">
                                    <i class="bi bi-folder"></i>
                                    <ul class="cats">
                                        <li>Arsip</li>
                                    </ul>
                                    <i class="bi bi-tags"></i>
                                    <ul class="tags">
                                        {{$dokumenShow->kategori_document->name}}
                                    </ul>
                                </div>
                            </article>
                        @endforeach

                    </div>
                    <div class="col-lg-4">
                        <div class="sidebar">
                            <div class="sidebar-item categories">
                                <h3 class="sidebar-title">Kategori Dokumen Lainnya</h3>
                                <ul class="mt-3">
                                    @foreach($kategoriAll as $kategoriValue)
                                    <li><a href="{{ route('dokumen.show', ['dokuman' => base64_encode($kategoriValue->id)]) }}">{{ $kategoriValue->name }}</a></li>
                                    @endforeach
                                </ul>
                            </div><!-- End sidebar categories-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
