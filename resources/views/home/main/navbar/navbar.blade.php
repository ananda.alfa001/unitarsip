<section id="topbar" class="topbar d-flex align-items-center">
    <div class="container d-flex justify-content-center justify-content-md-between">
        <div class="contact-info d-flex align-items-center">
            <i class="d-flex align-items-center ms-4" style="color:#f8f8f8;">"Archivum Est Potentia"</i>
        </div>
        <div class="social-links d-none d-md-flex align-items-center">
            <div class="social-links d-none d-md-flex align-items-center">
                <a href="mailto:unitarsip@ub.ac.id"><i class="bi bi-envelope"></i> unitarsip@ub.ac.id</a>
                <a href="#"><i class="bi bi-phone"></i>+62 812-5968-9277</a>
                <a href="https://www.instagram.com/arsip.ub" class="instagram"><i class="bi bi-instagram"></i></a>
                <a href="{{ route('login') }}"><i class="bi bi-people"></i></a>
            </div>
        </div>
    </div>
</section><!-- End Top Bar -->
<header id="header" class="header d-flex align-items-center">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
        <a href="/" class="logo d-flex align-items-center">
            <img style="padding-top: 10px; padding-bottom: 10px; margin-right: 0px;"
                src="{{ asset('assets/home/img/logo/logo_ub.png') }}" alt="">
            <img style="padding-top: 5px; padding-bottom: 0px"
                src="{{ asset('assets/home/img/logo/logo_arsip_putih.png') }}" alt="">
        </a>
        <nav id="navbar" class="navbar">
            <ul>
                <li class="dropdown">
                    <a href="#" id="dropdownLink">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-building"
                            width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                            fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M3 21l18 0"></path>
                            <path d="M9 8l1 0"></path>
                            <path d="M9 12l1 0"></path>
                            <path d="M9 16l1 0"></path>
                            <path d="M14 8l1 0"></path>
                            <path d="M14 12l1 0"></path>
                            <path d="M14 16l1 0"></path>
                            <path d="M5 21v-16a2 2 0 0 1 2 -2h10a2 2 0 0 1 2 2v16"></path>
                        </svg>
                        <span>&nbsp;Profil</span>
                        <i class="bi bi-chevron-down dropdown-indicator"></i>
                    </a>
                    <ul>
                        <li><a href="{{ route('home.sejarah') }}">Sejarah Kearsipan UB</a></li>
                        <li><a href="{{ route('home.visimisi') }}">Visi Misi</a></li>
                        <li><a href="{{ route('home.organisasi') }}">Struktur Organisasi</a></li>
                        {{-- <li><a href="#">Program Kerja</a></li> --}}
                        {{-- <li><a href="#">Sumber Daya Manusia</a></li> --}}
                        <li><a href="{{ route('home.logo') }}">Arti Logo</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#clients">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-vocabulary"
                            width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                            fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path
                                d="M10 19h-6a1 1 0 0 1 -1 -1v-14a1 1 0 0 1 1 -1h6a2 2 0 0 1 2 2a2 2 0 0 1 2 -2h6a1 1 0 0 1 1 1v14a1 1 0 0 1 -1 1h-6a2 2 0 0 0 -2 2a2 2 0 0 0 -2 -2z">
                            </path>
                            <path d="M12 5v16"></path>
                            <path d="M7 7h1"></path>
                            <path d="M7 11h1"></path>
                            <path d="M16 7h1"></path>
                            <path d="M16 11h1"></path>
                            <path d="M16 15h1"></path>
                        </svg>
                        <span>&nbsp;Jaminan Mutu</span>
                        <i class="bi bi-chevron-down dropdown-indicator"></i></a>
                    <ul>
                        <li><a href="{{ route('home.audit') }}">Audit</a></li>
                        <li><a
                            href="{{ route('dokumen.show', ['dokuman' => base64_encode(1)]) }}">Program kerja</a>
                        </li>
                        <li><a
                                href="{{ route('dokumen.show', ['dokuman' => base64_encode(3)]) }}">Tinjauan Manajemen</a>
                        </li>
                        <li><a
                                href="{{ route('dokumen.show', ['dokuman' => base64_encode(4)]) }}">Renstra</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" id="dropdownLink">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-redux"
                            width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                            fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path
                                d="M16.54 7c-.805 -2.365 -2.536 -4 -4.54 -4c-2.774 0 -5.023 2.632 -5.023 6.496c0 1.956 1.582 4.727 2.512 6">
                            </path>
                            <path
                                d="M4.711 11.979c-1.656 1.877 -2.214 4.185 -1.211 5.911c1.387 2.39 5.138 2.831 8.501 .9c1.703 -.979 2.875 -3.362 3.516 -4.798">
                            </path>
                            <path
                                d="M15.014 19.99c2.511 0 4.523 -.438 5.487 -2.1c1.387 -2.39 -.215 -5.893 -3.579 -7.824c-1.702 -.979 -4.357 -1.235 -5.927 -1.07">
                            </path>
                            <path
                                d="M10.493 9.862c.48 .276 1.095 .112 1.372 -.366a1 1 0 0 0 -.367 -1.365a1.007 1.007 0 0 0 -1.373 .366a1 1 0 0 0 .368 1.365z">
                            </path>
                            <path d="M9.5 15.5m-1 0a1 1 0 1 0 2 0a1 1 0 1 0 -2 0"></path>
                            <path d="M15.5 14m-1 0a1 1 0 1 0 2 0a1 1 0 1 0 -2 0"></path>
                        </svg>
                        <span>&nbsp;Layanan</span>
                        <i class="bi bi-chevron-down dropdown-indicator"></i></a>
                    <ul>
                        <li><a href="{{ route('home.pembinaan.kearsipan') }}">Pembinaan Kearsipan</a></li>
                        <li><a href="{{ route('home.pengelolaan.kearsipan.statis') }}">Pengelolaan Arsip Statis</a></li>
                        <li><a href="{{ route('home.pengelolaan.akuisisi.arsip') }}">Akuisisi Arsip</a></li>
                        <li><a href="{{ route('home.pelayanan.peminjaman.arsip') }}">Peminjaman Arsip</a></li>
                        <li><a href="{{ route('home.alihmedia.arsip') }}">Alih Media dan Digitalisasi</a></li>
                        <li><a href="{{ route('home.penyelamatan.arsip') }}">Perbaikan Dan Pelestarian Arsip</a></li>
                        <li><a href="{{ route('home.pemusnahan.arsip') }}">Pemusnahan Arsip</a></li>
                        <li><a href="{{ route('home.stiudibanding.arsip') }}">Kunjungan</a></li>
                        <li><a href="{{ route('survey.index') }}">Survey Kearsipan</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" id="dropdownLink">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-ad-2"
                            width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                            stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M11.933 5h-6.933v16h13v-8"></path>
                            <path d="M14 17h-5"></path>
                            <path d="M9 13h5v-4h-5z"></path>
                            <path d="M15 5v-2"></path>
                            <path d="M18 6l2 -2"></path>
                            <path d="M19 9h2"></path>
                        </svg>
                        <span>&nbsp;Publikasi</span>
                        <i class="bi bi-chevron-down dropdown-indicator"></i></a>
                    <ul>
                        <li class="dropdown">
                            <a href="#" id="dropdownLink"><span>Peraturan</span>
                                <i class="bi bi-chevron-down dropdown-indicator"></i></a>
                            <ul>
                                <li class="dropdown">
                                    <a href="#" id="dropdownLink">
                                        <span>UU</span>
                                        <i class="bi bi-chevron-down dropdown-indicator"></i></a>
                                    <ul>
                                        <li><a href="#">UU</a></li>
                                        <li><a href="#">Peraturan Menteri</a></li>
                                        <li><a href="#">Peraturan Pemerintah</a></li>
                                        <li><a href="#">Keputusan/Pepres</a></li>
                                        <li><a href="#">Peraturan Kepala ANRI</a></li>
                                        <li><a href="#">Keputusan Rektor</a></li>
                                        <li><a
                                            href="{{ route('dokumen.show', ['dokuman' => base64_encode(6)]) }}">Peraturan Rektor</a>
                                    </li>
                                    </ul>
                                </li>
                                <li><a href="{{ route('dokumen.show', ['dokuman' => base64_encode(2)]) }}">SOP</a></li>
                            </ul>
                        </li>
                        <li><a
                                href="{{ route('dokumen.show', ['dokuman' => base64_encode(5)]) }}">Peraturan Umum</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" id="dropdownLink">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-4chan"
                            width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                            stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path
                                d="M14 11s6.054 -1.05 6 -4.5c-.038 -2.324 -2.485 -3.19 -3.016 -1.5c0 0 -.502 -2 -2.01 -2c-1.508 0 -2.984 3 -.974 8z">
                            </path>
                            <path
                                d="M13.98 11s6.075 -1.05 6.02 -4.5c-.038 -2.324 -2.493 -3.19 -3.025 -1.5c0 0 -.505 -2 -2.017 -2c-1.513 0 -3 3 -.977 8z">
                            </path>
                            <path
                                d="M13 13.98l.062 .309l.081 .35l.075 .29l.092 .328l.11 .358l.061 .188l.139 .392c.64 1.73 1.841 3.837 3.88 3.805c2.324 -.038 3.19 -2.493 1.5 -3.025l.148 -.045l.165 -.058a4.13 4.13 0 0 0 .098 -.039l.222 -.098c.586 -.28 1.367 -.832 1.367 -1.777c0 -1.513 -3 -3 -8 -.977z">
                            </path>
                            <path
                                d="M10.02 13l-.309 .062l-.35 .081l-.29 .075l-.328 .092l-.358 .11l-.188 .061l-.392 .139c-1.73 .64 -3.837 1.84 -3.805 3.88c.038 2.324 2.493 3.19 3.025 1.5l.045 .148l.058 .165l.039 .098l.098 .222c.28 .586 .832 1.367 1.777 1.367c1.513 0 3 -3 .977 -8z">
                            </path>
                            <path
                                d="M11 10.02l-.062 -.309l-.081 -.35l-.075 -.29l-.092 -.328l-.11 -.358l-.128 -.382l-.148 -.399c-.658 -1.687 -1.844 -3.634 -3.804 -3.604c-2.324 .038 -3.19 2.493 -1.5 3.025l-.148 .045l-.164 .058a4.13 4.13 0 0 0 -.1 .039l-.22 .098c-.588 .28 -1.368 .832 -1.368 1.777c0 1.513 3 3 8 .977z">
                            </path>
                        </svg>
                        &nbsp;Donasi Arsip Brawijaya
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" id="dropdownLink">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-history-toggle"
                            width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                            stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M10 20.777a8.942 8.942 0 0 1 -2.48 -.969"></path>
                            <path d="M14 3.223a9.003 9.003 0 0 1 0 17.554"></path>
                            <path d="M4.579 17.093a8.961 8.961 0 0 1 -1.227 -2.592"></path>
                            <path d="M3.124 10.5c.16 -.95 .468 -1.85 .9 -2.675l.169 -.305"></path>
                            <path d="M6.907 4.579a8.954 8.954 0 0 1 3.093 -1.356"></path>
                            <path d="M12 8v4l3 3"></path>
                        </svg>
                        <span>&nbsp;UB Dalam Sejarah</span>
                        <i class="bi bi-chevron-down dropdown-indicator"></i></a>
                    <ul>
                        <li><a href="#">Arsip Stais</a></li>
                        <li><a href="#">Arsip Dinamis</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
        <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>
    </div>
</header>
