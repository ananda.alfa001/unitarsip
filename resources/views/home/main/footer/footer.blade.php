<!-- ======= Footer ======= -->
<footer id="footer" class="footer">

    <div class="container">
        <div class="row gy-4">
            <div class="col-lg-5 col-md-12 footer-info">
                <a href="/" class="logo d-flex align-items-center">
                    <span><img src="{{ asset('assets/home/img/logo/logo_ub.png') }}" alt=""></span>
                    <span><img src="{{ asset('assets/home/img/logo/logo_arsip_putih.png') }}"
                            alt=""></span>
                </a>
                <p>Mewujudkan penjaminan mutu secara berkelanjutan dan upaya mempertahankan akreditasi nasional
                    maupun internasional, Universitas Brawijaya memerlukan sistem pengendalian dokumen dan rekaman
                    yang baik.</p>
                <div class="social-links d-flex mt-4">
                    <a href="https://www.instagram.com/arsip.ub"class="instagram"><i
                            class="bi bi-instagram"></i></a>
                </div>
            </div>

            <div class="col-lg-2 col-6 footer-links">
                <h4></h4>
            </div>

            <div class="col-lg-2 col-6 footer-links">
                <h4></h4>
            </div>

            <div class="col-lg-3 col-md-12 footer-contact text-center text-md-start">
                <h4>Alamat Kantor</h4>
                Gedung Perpustakaan Lt.5, Jl. Veteran, Ketawang Gede, Kecamatan Lowokwaru<br>
                Kota Malang<br>65145<br>
                Jawa Timur
                </p>
            </div>
        </div>
    </div>

    <div class="container mt-4">
        <div class="copyright">
            &copy; Copyright 2023 <strong><span>unitarsip.ub.ac.id</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/impact-bootstrap-business-website-template/ -->
            Developt by <a href="https://unitarsip.ub.ac.id/">Unit Kearsipan I UB feat <a
                    href="https://ciptanesia.id/">Ciptanesia.id</a> <br></a> & Designed by <a
                href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
    </div>
</footer>
