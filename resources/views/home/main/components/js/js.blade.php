 <!-- Vendor JS Files -->
 <script src="{{ asset('assets/home/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
 <script src="{{ asset('assets/home/vendor/aos/aos.js') }}"></script>
 <script src="{{ asset('assets/home/vendor/glightbox/js/glightbox.min.js') }}"></script>
 <script src="{{ asset('assets/home/vendor/purecounter/purecounter_vanilla.js') }}"></script>
 <script src="{{ asset('assets/home/vendor/swiper/swiper-bundle.min.js') }}"></script>
 <script src="{{ asset('assets/home/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
 <script src="{{ asset('assets/home/vendor/php-email-form/validate.js') }}"></script>
 <script src="{{ asset('vendor/sweetalert/sweetalert.all.js') }}"></script>
 <!-- Template Main JS File -->
 <script src="{{ asset('assets/home/js/main.js') }}"></script>
 <script>
    document.addEventListener("DOMContentLoaded", function() {
    var dropdownLink = document.getElementById("dropdownLink");

    dropdownLink.addEventListener("click", function(event) {
        event.preventDefault();
    });
});
 </script>
 @yield('js')
