<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('home.main.components.license.license')
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Unit Kearsipan I - Universitas Brawijaya</title>
    <meta name="description" content="Kearsipan Universitas Brawijaya">
    <meta name="keywords"
        content="kearsipan,arsip, unibersitas brawijaya,arsipub, arsip ub, kearsipan ub, kearsipan universitas brawijaya, arsip satu, pusat arsip ub">

    <!-- SOCIAL MEDIA META -->
    <meta property="og:title" content="Unit Kearsipan I - Universitas Brawijaya">
    <meta property="og:description"
        content="Kearsipan I Universitas Brawijaya yang berkualitas (tertata, tersimpan, dan terdaftar) terintegrasi, berdayaguna, dan bernilaiguna.">
    <meta property="og:image" content="{{ asset('assets/home/img/logo/logo.png') }}">
    <meta property="og:url" content="https://unitarsip.ub.ac.id/">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Unit Kearsipan I">
    <meta name="twitter:card" content="Unit Kearsipan I">

    <!-- SEO META -->
    <meta name="google-site-verification" content="">

    <!-- Additional Meta Tags -->
    <meta name="robots"
        content="kearsipan,arsip, unibersitas brawijaya,arsipub, arsip ub, kearsipan ub, kearsipan universitas brawijaya, arsip satu, pusat arsip ub">
    <meta name="author" content="Ciptanesia.id">
    <meta name="googlebot" content="kearsipan,arsip, unibersitas brawijaya,arsipub, arsip ub, kearsipan ub, kearsipan universitas brawijaya, arsip satu, pusat arsip ub">
    @include('home.main.components.style.style')
    <!-- =======================================================
    * Template Name: Impact
    * Updated: Mar 10 2023 with Bootstrap v5.2.3
    * Template URL: https://bootstrapmade.com/impact-bootstrap-business-website-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>
<body>
    @include('sweetalert::alert')
    @include('home.main.navbar.navbar')
    {{-- ======================= --}}
    @yield('content')
    {{-- ======================= --}}
    @include('home.main.footer.footer')
    @include('home.main.loader.loader')
    @include('home.main.components.js.js')
</body>
</html>
