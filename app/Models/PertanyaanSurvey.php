<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PertanyaanSurvey
 * 
 * @property int $id
 * @property string $pertanyaan
 * @property int $type
 * @property int $urutan
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|JawabanSurvey[] $jawaban_surveys
 * @property Collection|OptionSurvey[] $option_surveys
 *
 * @package App\Models
 */
class PertanyaanSurvey extends Model
{
	protected $table = 'pertanyaan_survey';

	protected $casts = [
		'type' => 'int',
		'urutan' => 'int'
	];

	protected $fillable = [
		'pertanyaan',
		'type',
		'urutan'
	];

	public function jawaban_surveys()
	{
		return $this->hasMany(JawabanSurvey::class, 'pertanyaan_id');
	}

	public function option_surveys()
	{
		return $this->hasMany(OptionSurvey::class, 'pertanyaan_id');
	}
}
