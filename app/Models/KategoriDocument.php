<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class KategoriDocument
 * 
 * @property int $id
 * @property string $name
 * @property int $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|DocumentLibrary[] $document_libraries
 *
 * @package App\Models
 */
class KategoriDocument extends Model
{
	protected $table = 'kategori_document';

	protected $casts = [
		'status' => 'int'
	];

	protected $fillable = [
		'name',
		'status'
	];

	public function document_libraries()
	{
		return $this->hasMany(DocumentLibrary::class);
	}
}
