<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PenjawabSurvey
 * 
 * @property int $id
 * @property string $nama
 * @property string $asal
 * @property string $email
 * @property string $no_hp
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|JawabanSurvey[] $jawaban_surveys
 *
 * @package App\Models
 */
class PenjawabSurvey extends Model
{
	protected $table = 'penjawab_survey';

	protected $fillable = [
		'nama',
		'asal',
		'email',
		'no_hp'
	];

	public function jawaban_surveys()
	{
		return $this->hasMany(JawabanSurvey::class, 'penjawab_id');
	}
}
