<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Kategori
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Collection|Beritum[] $berita
 *
 * @package App\Models
 */
class Kategori extends Model
{
	protected $table = 'kategori';

	protected $fillable = [
		'name'
	];

	public function berita()
	{
		return $this->hasMany(Berita::class);
	}
}
