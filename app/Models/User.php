<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Company $company
 * @property Collection|Role[] $roles
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use HasFactory;
	use SoftDeletes;
	protected $table = 'users';

	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'email',
		'email_verified_at',
		'active',
		'verify_key',
		'password',
		'remember_token'
	];

	public function roles()
	{
		return $this->belongsToMany(Role::class)
					->withPivot('id', 'deleted_at')
					->withTimestamps();
	}

    //Pengecekan Role User
    public function hasAnyRole(...$roles)
    {
        return $this->roles()->whereIn('name', ...$roles)->exists();
    }
    //Ambil Role user Bosque.. (Opotional bila mau check role kita sebagai apa di menu/sub menu/ .etc)
    public function GetRole()
    {
        return $this->roles()->pluck('name')->first();
    }
    //Get Sub Menu yang dimiliki Role
    public function GetMenus(...$menu)
    {
            return $this->roles()->whereHas('menu', function ($query) use ($menu) {
                $query->whereIn('title', $menu);
            })->exists();
    }
    /*
    Catatan Nanda : Hati-hati, kamu tidak tahu apa yang kamu hapus :)).
    [MCG 2023]
    */
}
