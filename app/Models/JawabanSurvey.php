<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class JawabanSurvey
 *
 * @property int $id
 * @property int $penjawab_id
 * @property int $pertanyaan_id
 * @property string $jawaban
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property PenjawabSurvey $penjawab_survey
 * @property PertanyaanSurvey $pertanyaan_survey
 *
 * @package App\Models
 */
class JawabanSurvey extends Model
{
	protected $table = 'jawaban_survey';

	protected $casts = [
		'penjawab_id' => 'int',
		'pertanyaan_id' => 'int',
		'point' => 'int'
	];

	protected $fillable = [
		'penjawab_id',
		'pertanyaan_id',
		'jawaban',
		'point'
	];

	public function penjawab_survey()
	{
		return $this->belongsTo(PenjawabSurvey::class, 'penjawab_id');
	}

	public function pertanyaan_survey()
	{
		return $this->belongsTo(PertanyaanSurvey::class, 'pertanyaan_id');
	}
}
