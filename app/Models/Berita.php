<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Beritum
 *
 * @property int $id
 * @property string $judul
 * @property string|null $thumbnail
 * @property string $konten
 * @property int $pembaca
 * @property int $kategori_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Kategori $kategori
 *
 * @package App\Models
 */
class Berita extends Model
{
	protected $table = 'berita';

	protected $casts = [
		'pembaca' => 'int',
		'kategori_id' => 'int'
	];

	protected $fillable = [
		'judul',
		'thumbnail',
		'konten',
		'pembaca',
		'publish',
		'author',
		'kategori_id'
	];

	public function kategori()
	{
		return $this->belongsTo(Kategori::class);
	}
}
