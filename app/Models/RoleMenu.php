<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RoleMenu
 * 
 * @property int $role_id
 * @property int $menu_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Menu $menu
 * @property Role $role
 *
 * @package App\Models
 */
class RoleMenu extends Model
{
	protected $table = 'role_menu';
	public $incrementing = false;

	protected $casts = [
		'role_id' => 'int',
		'menu_id' => 'int'
	];

	protected $fillable = [
		'role_id',
		'menu_id'
	];

	public function menu()
	{
		return $this->belongsTo(Menu::class);
	}

	public function role()
	{
		return $this->belongsTo(Role::class);
	}
}
