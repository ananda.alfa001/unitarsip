<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Menu
 *
 * @property int $id
 * @property string $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property Collection|Role[] $roles
 *
 * @package App\Models
 */
class Menu extends Model
{
	protected $table = 'menu';

	protected $fillable = [
		'title'
	];

	public function roles()
	{
		return $this->belongsToMany(Role::class, 'role_menu')
					->withTimestamps();
	}
}
