<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentLibrary
 *
 * @property int $id
 * @property string $judul
 * @property string|null $document
 * @property string $desc
 * @property int $penglihat
 * @property int $kategori_document_id
 * @property bool $publish
 * @property string|null $author
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property KategoriDocument $kategori_document
 *
 * @package App\Models
 */
class DocumentLibrary extends Model
{
	protected $table = 'document_library';

	protected $casts = [
		'penglihat' => 'int',
		'kategori_document_id' => 'int',
		'publish' => 'bool'
	];

	protected $fillable = [
		'judul',
		'document',
		'desc',
		'penglihat',
		'kategori_document_id',
		'publish',
		'author'
	];

	public function kategori_document()
	{
		return $this->belongsTo(KategoriDocument::class, 'kategori_document_id', 'id');
	}
}
