<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OptionSurvey
 *
 * @property int $id
 * @property int $pertanyaan_id
 * @property string $option
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property PertanyaanSurvey $pertanyaan_survey
 *
 * @package App\Models
 */
class OptionSurvey extends Model
{
	protected $table = 'option_survey';

	protected $casts = [
		'pertanyaan_id' => 'int',
		'point' => 'int'
	];

	protected $fillable = [
		'pertanyaan_id',
		'option',
		'point'
	];

	public function pertanyaan_survey()
	{
		return $this->belongsTo(PertanyaanSurvey::class, 'pertanyaan_id');
	}
}
