<?php

namespace App\Http\Controllers\Menu\Berita\UpdateBerita;

use App\Http\Controllers\Controller;
use App\Models\Berita;
use App\Models\Kategori;
use Illuminate\Support\Facades\Storage;
use App\Models\Role;
use Illuminate\Support\Str;
use App\Models\RoleUser;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Validation\Rule;

class UpdateBeritaController extends Controller
{
    private $user;
    private $kategori;
    private $roleuser;
    private $berita;
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = new User();
        $this->kategori = new Kategori();
        $this->roleuser = new RoleUser();
        $this->berita = new Berita();
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $kategori = $this->kategori::get();
        return view('menu.berita.updateberita.index',compact('kategori'));
    }
    /**
     * Display a listing of the resource.
     */
    public function index_datatable(Request $request)
    {
        //Datatable Manual : Create by Ananda & Doc. Vuexy
        $columns = [
            'id',
            'judul',
            'kategori.kategori_name',
        ];
        $totalData = Berita::count();
        $search = $request->input('search.value');
        $query = $this->berita::with('kategori')->select('berita.*');
        if (!empty($search)) {
            $query->where(function ($q) use ($columns, $search) {
                foreach ($columns as $column) {
                    if (strpos($column, '.') !== false) {
                        // Kolom relasional
                        [$relation, $column] = explode('.', $column);
                        $q->orWhereHas($relation, function ($query) use ($column, $search) {
                            $query->where($column, 'LIKE', "%$search%");
                        });
                    } else {
                        // Kolom non-relasional
                        $q->orWhere($column, 'LIKE', "%$search%");
                    }
                }
            });
        }
        $totalFiltered = $query->count();
        $start = $request->input('start', 0);
        $length = $request->input('length', 10);
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir', 'asc');
        $query->offset($start)->limit($length)->orderBy($order, $dir);
        $data = [];
        foreach ($query->get() as $berita) {
            $kategori = $berita->kategori;
            $kategoriName = $kategori ? $berita->kategori->name : '-';
            $kategoriChoice = $kategori ? $berita->kategori->id : '-';
            $data[] = [
                'id' => $berita->id,
                'judul' => $berita->judul,
                'kategori' =>  $kategoriName,
                'pembaca' => $berita->pembaca ?? '0',
                'status' => $berita->publish == '0' ? 'Publish' : 'Draft',
                'actions' => '<button class="btn btn-primary btn-edit" data-bs-target="#EditBeritaModal" data-bs-toggle="modal" data-id="'.$berita->id.'" data-judul="'.$berita->judul.'" data-publish="'.$berita->publish.'" data-kategori="'.$kategoriChoice.'" data-konten="'.base64_encode($berita->konten).'" ">Edit</button>
                            <button class="btn btn-danger btn-delete" data-id="'.$berita->id.'">Delete</button>',
            ];
        }
        $response = [
            'draw' => intval($request->input('draw', 1)),
            'recordsTotal' => $totalData,
            'recordsFiltered' => $totalFiltered,
            'data' => $data,
        ];
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => ['required', 'string', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
            'kategori_id' => ['required', 'string', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
            'publish' => ['required'],
            'newscontent' => ['required'],
            'tumbnail' => ['required'],
        ]);
        if ($validator->fails()) {
            //Validator
            Alert::warning('Input Belum Lengkap!',  $validator->errors()->all());
            return Redirect()->back();
        } else {
            try {
                //Save Image
                $FileRequest = $request->file('tumbnail');
                //Get Ext
                $file['ext'] = $FileRequest->getClientOriginalExtension();
                //Naming File (Bisa Disesuaikan berdasarkan Kebutuhan Penamaan)
                $file['title'] = date('dmY') . '.' . $file['ext'];
                //path
                $path = 'publikasi/berita/'. date('d-m-y') .'/'. $request->judul;
                $FileRequest->move(public_path($path), $file['title']);
                //Result
                $tumb = $path.'/'.$file['title'];
                DB::transaction(function() use ($request, $tumb) {
                    //Create User
                    $data = new $this->berita;
                    $data->judul = $request->judul;
                    $data->kategori_id = $request->kategori_id;
                    $data->thumbnail = $tumb;
                    $data->konten = $request->newscontent;
                    $data->publish = $request->publish;
                    $data->author = Auth::user()->name;
                    $data->save();
                });
                Alert::success('Success', 'Berita Saved!');
                return Redirect()->route('berita.index');
            } catch (Exception $e) {
                throw $e;
                DB::rollback();
                Alert::error('Error', $e->getMessage());
                return Redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, string $id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            'judul' => ['required', 'string', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
            'kategori_id' => ['required', 'string', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
            'publish' => ['required'],
            'newscontent' => ['required'],
        ]);
        if ($validator->fails()) {
            //Validator
            Alert::warning('Input Belum Lengkap!',  $validator->errors()->all());
            return Redirect()->back();
        } else {
            try {
                $berita = $this->berita::find($request->berita_id);
                if (!empty($berita)) {
                    // User exists
                    DB::transaction(function () use ($request, $berita) {
                        // Save Image
                        if ($request->hasFile('tumbnail')) {
                            // Delete old tumbnail

                            Storage::delete($berita->thumbnail);
                            $FileRequest = $request->file('tumbnail');
                            // Get Ext
                            $file['ext'] = $FileRequest->getClientOriginalExtension();
                            // Naming File (Bisa Disesuaikan berdasarkan Kebutuhan Penamaan)
                            $file['title'] = date('dmY') . '.' . $file['ext'];
                            // Path
                            $path = 'publikasi/berita/'. date('d-m-y') .'/'. $request->judul;
                            $FileRequest->move(public_path($path), $file['title']);
                            // Result
                            $tumb = $path . '/' . $file['title'];
                        } else {
                            $tumb = $berita->tumbnail;
                        }
                        // Update Berita
                        $berita->judul = $request->judul;
                        $berita->kategori_id = $request->kategori_id;
                        $berita->thumbnail = $tumb;
                        $berita->konten = $request->newscontent;
                        $berita->publish = $request->publish;
                        $berita->author = Auth::user()->name;
                        $berita->save();
                    });
                    Alert::success('Success', 'Berita Updated!');
                    return Redirect()->route('berita.index');
                } else {
                    Alert::success('Warning', 'Berita Tidak Ada!');
                    return Redirect()->route('berita.index');
                }
            } catch (Exception $e) {
                throw $e;
                DB::rollback();
                Alert::error('Error', $e->getMessage());
                return Redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if ($id == null) {
            return redirect()->back()->with('error', 'Id Tidak Tersedia');
        } else {
            try {
                DB::transaction(function() use ($id) {
                    $data = $this->berita::find($id);
                    $data->delete();
                });
                return response()->json(['success' => 'Data Deleted!'], 200);
            } catch (Exception $e) {
                throw $e;
                DB::rollback();
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }
    }
}
