<?php

namespace App\Http\Controllers\Menu\Berita\UpdateKategori;

use App\Http\Controllers\Controller;
use App\Models\Kategori;
use App\Models\Menu;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RoleMenu;
use App\Models\SubMenu;
use App\Models\SubMenuRole;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class UpdateKategoriController extends Controller
{
    private $rolemenu;
    private $kategori;
    public function __construct()
    {
        $this->middleware('auth');
        $this->kategori = new Kategori();
        $this->menu = new Menu();
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kategoriall = $this->kategori::with('berita')->get();
        return view('menu.berita.updatekategori.index',compact('kategoriall'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'kategori' => 'required','string','unique:roles.name'
        ]);
        if ($validator->fails()) {
            //Validator
            Alert::warning('Input Belum Lengkap!',  $validator->errors()->all());
            return Redirect()->back();
        } else {
            try {
                DB::transaction(function() use ($request) {
                    $newRole = new $this->kategori;
                    $newRole->name = $request->kategori;
                    $newRole->save();
                });
                Alert::success('Success', 'Kategori Saved!');
                return Redirect()->route('kategori-berita.index');
            } catch (Exception $e) {
                throw $e;
                DB::rollback();
                Alert::error('Error', $e->getMessage());
                return Redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
