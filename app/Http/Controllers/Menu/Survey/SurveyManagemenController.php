<?php

namespace App\Http\Controllers\Menu\Survey;

use App\Http\Controllers\Controller;
use App\Models\JawabanSurvey;
use App\Models\PenjawabSurvey;
use App\Models\PertanyaanSurvey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\DB;
use DataTables;
use Exception;

class SurveyManagemenController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pertanyaan = PertanyaanSurvey::get();
        return view('menu.survey.survey', compact('pertanyaan'));
    }

/**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = PenjawabSurvey::with('jawaban_surveys')->orderBy('id', 'DESC');
        return DataTables::eloquent($data)
            ->addIndexColumn()
            ->addColumn('pertanyaan1', function ($data) {
                return $this->getJawaban($data, 0);
            })
            ->addColumn('pertanyaan2', function ($data) {
                return $this->getJawaban($data, 1);
            })
            ->addColumn('pertanyaan3', function ($data) {
                return $this->getJawaban($data, 2);
            })
            ->addColumn('pertanyaan4', function ($data) {
                return $this->getJawaban($data, 3);
            })
            ->addColumn('pertanyaan5', function ($data) {
                return $this->getJawaban($data, 4);
            })
            ->addColumn('pertanyaan6', function ($data) {
                return $this->getJawaban($data, 5);
            })
            ->addColumn('pertanyaan7', function ($data) {
                return $this->getJawaban($data, 6);
            })
            ->addColumn('pertanyaan8', function ($data) {
                return $this->getJawaban($data, 7);
            })
            ->addColumn('pertanyaan9', function ($data) {
                return $this->getJawaban($data, 8);
            })
            ->addColumn('pertanyaan10', function ($data) {
                return $this->getJawaban($data, 9);
            })
            ->addColumn('pertanyaan11', function ($data) {
                return $this->getJawaban($data, 10);
            })
            ->addColumn('pertanyaan12', function ($data) {
                return $this->getJawaban($data, 11);
            })
            ->addColumn('pertanyaan13', function ($data) {
                return $this->getJawaban($data, 12)." Bintang";
            })
            ->addColumn('pertanyaan14', function ($data) {
                return "'".$this->getJawaban($data, 13). "'";
            })
            ->addColumn('actions', function ($data) {
                return '<button class="btn btn-danger btn-delete" data-id="'.$data->id.'">Delete</button>';
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    private function getJawaban($data, $index) {
        if(isset($data->jawaban_surveys[$index])) {
            return $data->jawaban_surveys[$index]->jawaban;
        } else {
            return "0";
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            DB::transaction(function () use ($id) {
                JawabanSurvey::where('penjawab_id' , $id)->delete();
                PenjawabSurvey::where('id' , $id)->delete();
            });
            Alert::success('Success', 'Survey Deleted!');
            return redirect()->back();
        } catch (Exception $e) {
            throw $e;
            Alert::error('Error', $e->getMessage());
            return Redirect()->back();
        }
    }
}
