<?php

namespace App\Http\Controllers\Menu\Survey;

use App\Http\Controllers\Controller;
use App\Models\JawabanSurvey;
use App\Models\OptionSurvey;
use App\Models\PenjawabSurvey;
use App\Models\PertanyaanSurvey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Session;
use DataTables;
use Exception;
use PDO;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('home.homepage.layanan.survey.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        try {
            $id = session('id');
            if($id == null) {
                Alert::error('Failed!', 'Biodata tidak ditemukan!');
                return redirect()->route('survey.create');
            } else{
                $pertanyaan = PertanyaanSurvey::with('option_surveys')->get();
                return view('home.homepage.layanan.survey.pertanyaan', compact('pertanyaan'));
            }
        } catch (Exception $e) {
            throw $e;
            Alert::error('Error', $e->getMessage());
            return Redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => ['required', 'string', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
            'asal' => ['required', 'string', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
            'email' => ['required', 'email'],
            'no_hp' => ['required', 'regex:/^[0-9\+\-\s]+$/'],
        ]);
        if ($validator->fails()) {
            //Validator
            Alert::warning('Input Belum Lengkap!',  $validator->errors()->all());
            return Redirect()->back();
        } else {
            $email = $request->email;
            try {
                DB::transaction(function () use ($request, $email) {
                    $data = PenjawabSurvey::updateOrCreate(
                        ['email' => $email],
                        [
                            'nama' => $request->nama,
                            'asal' => $request->asal,
                            'email' => $email,
                            'no_hp' => $request->no_hp
                        ]
                    );
                    session(['id' => $data->id]);
                });
                Alert::success('Success', 'Biodata Saved!');
                return redirect()->route('survey.create');
            } catch (Exception $e) {
                throw $e;
                Alert::error('Error', $e->getMessage());
                return Redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            //
        ]);
        if ($validator->fails()) {
            //Validator
            Alert::warning('Input Belum Lengkap!',  $validator->errors()->all());
            return Redirect()->back();
        } else {
            $id = session('id');
            if($id == null) {
                Alert::error('Failed!', 'Biodata tidak ditemukan!');
                return redirect()->route('survey.create');
            } else{
                try {
                    DB::transaction(function () use ($request, $id) {
                        foreach ($request->except(['_token', '_method']) as $pertanyaan_id => $jawaban) {
                            $point = OptionSurvey::where('pertanyaan_id', $pertanyaan_id)->where('option', $jawaban)->value('point') ?? 10;
                            JawabanSurvey::updateOrCreate(
                                [
                                    'penjawab_id' => $id,
                                    'pertanyaan_id' => $pertanyaan_id
                                ],
                                [
                                    'jawaban' => $jawaban,
                                    'point' => $point
                                ]
                            );
                        }
                    });
                    Session::forget('id');
                    Alert::success('Success', 'Survey Submitted!');
                    return redirect()->route('survey.index');
                } catch (Exception $e) {
                    throw $e;
                    Alert::error('Error', $e->getMessage());
                    return Redirect()->back();
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
