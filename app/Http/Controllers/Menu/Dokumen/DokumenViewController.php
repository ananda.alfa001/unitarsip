<?php

namespace App\Http\Controllers\Menu\Dokumen;

use App\Http\Controllers\Controller;
use App\Models\DocumentLibrary;
use App\Models\KategoriDocument;
use Illuminate\Http\Request;

class DokumenViewController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $kategori = KategoriDocument::find(base64_decode($id));
        $kategoriAll = KategoriDocument::all();
        $dokumen = DocumentLibrary::where('kategori_document_id', base64_decode($id))->get();
        return view('home.document.index', compact('dokumen','kategori','kategoriAll'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
