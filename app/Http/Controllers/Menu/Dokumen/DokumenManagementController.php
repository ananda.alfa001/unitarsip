<?php

namespace App\Http\Controllers\Menu\Dokumen;

use App\Http\Controllers\Controller;
use App\Models\DocumentLibrary;
use App\Models\KategoriDocument;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use DataTables;
use Exception;

class DokumenManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $category = KategoriDocument::get();
        return view('menu.document.management.document', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = DocumentLibrary::with('kategori_document');
        return DataTables::eloquent($data)
            ->addIndexColumn()
            ->editColumn('publish', function ($data) {
                $result = 0;
                if($data->publish == 1){
                    $result = "Publish";
                } else {
                    $result = "Draft";
                }
                return $result;
            })
            ->addColumn('actions', function ($berita) {
                return '<button class="btn btn-danger btn-delete" data-id="'.$berita->id.'">Delete</button>';
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'judul' => ['required', 'string', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
            'kategori_id' => ['required', 'string', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
            'desc' => ['required'],
            'publish' => ['required'],
            'document' => ['required'],
        ]);
        if ($validator->fails()) {
            //Validator
            Alert::warning('Input Belum Lengkap!',  $validator->errors()->all());
            return Redirect()->back();
        } else {
            try {
                //Save Image
                $FileRequest = $request->file('document');
                //Get Ext
                $file['ext'] = $FileRequest->getClientOriginalExtension();
                //Naming File (Bisa Disesuaikan berdasarkan Kebutuhan Penamaan)
                $file['title'] = date('dmY') . '.' . $file['ext'];
                //path
                $path = 'publikasi/document/'. date('d-m-y') .'/'. $request->judul;
                $FileRequest->move(public_path($path), $file['title']);
                //Result
                $doc = $path.'/'.$file['title'];
                DB::transaction(function() use ($request, $doc) {
                    //Create User
                    $data = new DocumentLibrary();
                    $data->judul = $request->judul;
                    $data->kategori_document_id = $request->kategori_id;
                    $data->document = $doc;
                    $data->desc = $request->desc;
                    $data->publish = $request->publish;
                    $data->author = Auth::user()->name;
                    $data->save();
                });
                Alert::success('Success', 'Dokumen Saved!');
                return Redirect()->route('dokumen-management.index');
            } catch (Exception $e) {
                throw $e;
                DB::rollback();
                Alert::error('Error', $e->getMessage());
                return Redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            DB::transaction(function () use ($id) {
                $document = DocumentLibrary::findOrFail($id);
                if (file_exists(public_path($document->document))) {
                    unlink(public_path($document->document));
                }
                $document->delete();
            });
            Alert::success('Success', 'Dokumen Deleted!');
            return redirect()->back();
        } catch (Exception $e) {
            throw $e;
            Alert::error('Error', $e->getMessage());
            return Redirect()->back();
        }
    }
}
