<?php

namespace App\Http\Controllers\Menu\UserManagement\UserRole;

use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Permission;
use App\Models\Role;
use App\Models\RoleMenu;
use App\Models\SubMenu;
use App\Models\SubMenuRole;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class UserRoleController extends Controller
{
    private $user;
    private $role;
    private $permission;
    private $menu;
    private $rolemenu;
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = new User();
        $this->role = new Role();
        $this->permission = new Permission();
        $this->rolemenu = new RoleMenu();
        $this->menu = new Menu();
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $role = $this->role::with('users')->get();
        $menu = $this->menu::with('roles')->get();
        return view('menu.usermanagement.userrole.index',compact('role','menu'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'rolename' => 'required','string','unique:roles.name'
        ]);
        if ($validator->fails()) {
            //Validator
            Alert::warning('Input Belum Lengkap!',  $validator->errors()->all());
            return Redirect()->back();
        } else {
            try {
                DB::transaction(function() use ($request) {
                    //Insert Role Baru dahulu
                    $newRole = new $this->role;
                    $newRole->name = $request->rolename;
                    $newRole->save();
                     // Mendapatkan nilai checkbox yang dipilih
                    $permissionsrolemenu = $request->input('permissions');
                    // Memproses data sesuai dengan nilai checkbox yang dipilih
                    foreach ($permissionsrolemenu as $permission) {
                        $rolemenu = $this->rolemenu::where('menu_id', $permission)->where('role_id',  $newRole->id)->first();
                        // Cek Tidak ada
                        if (empty($rolemenu)) {
                            // Attach (memasukkan) relasi antara Role dan SubMenu
                            $newRole->sub_menus()->attach($permission);
                        }
                    }
                });
                Alert::success('Success', 'User Saved!');
                return Redirect()->route('user-role.index');
            } catch (Exception $e) {
                throw $e;
                DB::rollback();
                Alert::error('Error', $e->getMessage());
                return Redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
