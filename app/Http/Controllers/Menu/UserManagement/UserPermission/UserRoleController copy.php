<?php

namespace App\Http\Controllers\Menu\UserManagement\UserRole;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Permission;
use App\Models\Role;
use App\Models\SubMenu;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class UserRoleController extends Controller
{
    private $user;
    private $company;
    private $role;
    private $submenu;
    private $permission;
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = new User();
        $this->company = new Company();
        $this->role = new Role();
        $this->permission = new Permission();
        $this->submenu = new SubMenu();
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = $this->user::get();
        $role = $this->role::with('users')->get();
        $submenu = $this->submenu::with('roles')->get();
        return view('menu.usermanagement.userrole.index',compact('user','role','submenu'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'rolename' => 'required','string','unique:roles.name'
        ]);
        if ($validator->fails()) {
            //Validator
            Alert::warning('Input Belum Lengkap!',  $validator->errors()->all());
            return Redirect()->back();
        } else {
            try {
                DB::transaction(function() use ($request) {
                    //Insert Role Baru dahulu
                    $newRole = new $this->role;
                    $newRole->name = $request->rolename;
                    $newRole->save();
                     // Mendapatkan nilai checkbox yang dipilih
                    $permissions = $request->input('permissions');
                    // Memproses data sesuai dengan nilai checkbox yang dipilih
                    foreach ($permissions as $permission) {
                        // Misalnya, pecah nilai permission menjadi submenu_id dan permission_type
                        list($submenuId, $permissionType) = explode(':', $permission);
                        // Cari berdasarkan permission_type
                        $permission = $this->permission::where('name', $permissionType)->first();
                        // Cari berdasarkan submenu_id
                        $submenu = $this->submenu::find($submenuId)->first();

                        // Cek jika Role dan permission ada
                        if ($submenu && $permission) {
                              // Attach (memasukkan) relasi antara Role dan Permission
                            $newRole->permissions()->attach($permission->id);
                            // Attach (memasukkan) relasi antara Role dan SubMenu
                            $submenu->roles()->attach($newRole->id);
                        }
                    }
                });
                Alert::success('Success', 'User Saved!');
                return Redirect()->route('user-role.index');
            } catch (Exception $e) {
                throw $e;
                DB::rollback();
                Alert::error('Error', $e->getMessage());
                return Redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
