<?php

namespace App\Http\Controllers\Menu\UserManagement\ListUser;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Support\Str;
use App\Models\RoleUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Validation\Rule;

class ListUserController extends Controller
{
    private $user;
    private $role;
    private $roleuser;
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = new User();
        $this->role = new Role();
        $this->roleuser = new RoleUser();
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $role = $this->role::get();
        return view('menu.usermanagement.userlist.index',compact('role'));
    }
    /**
     * Display a listing of the resource.
     */
    public function index_datatable(Request $request)
    {
        //Datatable Manual : Create by Ananda & Doc. Vuexy
        $columns = [
            'id',
            'name',
            'email',
        ];
        $totalData = User::count();
        $search = $request->input('search.value');
        $query = $this->user::with('roles')->select('users.*');
        if (!empty($search)) {
            $query->where(function ($q) use ($columns, $search) {
                foreach ($columns as $column) {
                    if (strpos($column, '.') !== false) {
                        // Kolom relasional
                        [$relation, $column] = explode('.', $column);
                        $q->orWhereHas($relation, function ($query) use ($column, $search) {
                            $query->where($column, 'LIKE', "%$search%");
                        });
                    } else {
                        // Kolom non-relasional
                        $q->orWhere($column, 'LIKE', "%$search%");
                    }
                }
            });
        }
        $totalFiltered = $query->count();
        $start = $request->input('start', 0);
        $length = $request->input('length', 10);
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir', 'asc');
        $query->offset($start)->limit($length)->orderBy($order, $dir);
        $data = [];
        foreach ($query->get() as $user) {
            $roles = $user->roles;
            $roleName = $roles->isNotEmpty() ? $roles->first()->name : '-';
            $roleChoice = $roles->isNotEmpty() ? $roles->first()->id : '-';
            $data[] = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'roles' =>  $roleName,
                'actions' => '<button class="btn btn-primary btn-edit" data-bs-target="#editUserModal" data-bs-toggle="modal" data-id="'.$user->id.'" data-name="'.$user->name.'" data-email="'.$user->email.'" data-roles="'.$roleChoice.'" ">Edit</button>
                            <button class="btn btn-danger btn-delete" data-id="'.$user->id.'">Delete</button>',
            ];
        }
        $response = [
            'draw' => intval($request->input('draw', 1)),
            'recordsTotal' => $totalData,
            'recordsFiltered' => $totalFiltered,
            'data' => $data,
        ];
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }
    /**
     * Show the form for creating a new company resource.
     */
    public function create_company(Request $request)
    {
        $validator = Validator::make($request->all(), [
            //
        ]);
        if ($validator->fails()) {
            //Validator
            Alert::error('Error','Data Tidak Lengkap!');
            return Redirect()->back();
        } else {
            try {
                DB::transaction(function() use ($request) {
                    $data = new $this->company();
                    $data->company_name = $request->CompanyName;
                    $data->company_code = $request->CompanyCode;
                    $data->save();
                });
                Alert::success('Success', 'Company Saved!');
                return Redirect()->route('user-list.index');
            } catch (Exception $e) {
                throw $e;
                DB::rollback();
                Alert::error('Error', $e->getMessage());
                return Redirect()->back();
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
            'role_id' => ['required', 'string', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
        ]);
        if ($validator->fails()) {
            //Validator
            Alert::warning('Input Belum Lengkap!',  $validator->errors()->all());
            return Redirect()->back();
        } else {
            try {
                DB::transaction(function() use ($request) {
                    //Create User
                    $data = new $this->user();
                    $data->name = $request->name;
                    $data->email = $request->email;
                    $data->active = 1;
                    $data->verify_key = Str::random(64);
                    $data->password = Hash::make($request->password);
                    $data->save();
                    //Assign Role id dari User
                    $role = new $this->roleuser();
                    $role->user_id = $data->id;
                    $role->role_id = $request->role_id;
                    $role->save();
                });
                Alert::success('Success', 'User Saved!');
                return Redirect()->route('user-list.index');
            } catch (Exception $e) {
                throw $e;
                DB::rollback();
                Alert::error('Error', $e->getMessage());
                return Redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, string $id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => ['required', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
            'name' => ['required', 'string', 'max:255', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($request->user_id, 'id'),],
            'role_id' => ['required', 'string', 'regex:/^[a-zA-Z0-9\s\-\.,]+$/'],
        ]);
        if ($validator->fails()) {
            //Validator
            Alert::warning('Input Belum Lengkap!',  $validator->errors()->all());
            return Redirect()->back();
        } else {
            try {
                $user = $this->user::find($request->user_id);
                if (!empty($user)) {
                    // User exists
                    DB::transaction(function () use ($request, $user) {
                        // Update User
                        $user->name = $request->name;
                        $user->email = $request->email;
                        $user->password = Hash::make($request->password);
                        $user->save();
                        // Update Role
                        $user->roles()->sync([$request->role_id]);
                    });
                    Alert::success('Success', 'User Updated!');
                    return Redirect()->route('user-list.index');
                } else {
                    Alert::success('Warning', 'User Not Found!');
                    return Redirect()->route('user-list.index');
                }
            } catch (Exception $e) {
                throw $e;
                DB::rollback();
                Alert::error('Error', $e->getMessage());
                return Redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if ($id == null) {
            return redirect()->back()->with('error', 'Id Tidak Tersedia');
        } else {
            try {
                DB::transaction(function() use ($id) {
                    $data = $this->user::find($id);
                    $data->delete();
                });
                return response()->json(['success' => 'Data Deleted!'], 200);
            } catch (Exception $e) {
                throw $e;
                DB::rollback();
                return response()->json(['error' => $e->getMessage()], 500);
            }
        }
    }
}
