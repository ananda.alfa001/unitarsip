<?php

namespace App\Http\Controllers;

use App\Models\Berita;
use App\Models\Kategori;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $kategori;
    private $berita;
    public function __construct()
    {
        $this->middleware('guest');
        $this->kategori = new Kategori();
        $this->berita = new Berita();
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kategori = $this->kategori::get();
        $berita =  $this->berita::where('publish', 1)->limit(6)->orderBy('created_at', 'DESC')->get();
        return view('home.index', compact('kategori','berita'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $beritaId = base64_decode($id);
        $berita = $this->berita::find($beritaId);
        $kategori = $this->kategori::get();
        $beritaUpdate = $this->berita::where('publish', 1)->limit(6)->orderBy('created_at', 'DESC')->get();
        // Update Pembaca
        $berita->pembaca += 1;
        $berita->save();
        $pembaca = $berita->pembaca;
        return view('home.news', compact('berita', 'beritaUpdate', 'kategori', 'pembaca'));

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
