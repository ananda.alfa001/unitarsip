<?php

namespace App\Http\Controllers\Auth;

use Exception;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'emailusername' => 'required|min:2|max:255|string',
            'password' => 'required|min:2|max:255|string|regex:/^[a-zA-Z0-9\s\-\.,]+$/',
        ]);
        if ($validator->fails()) {
            Alert::warning('Input Salah!',  $validator->errors()->all());
            return view('auth.login');
        } else {
            try {
                // $auth = false;
                $remember = $request->input('remember');
                //PROCCESS Login
                $email = User::where('email', $request->emailusername)->value('email');
                if (isset($email)) {
                    if (Auth::guard('web')->attempt(['email' => $email, 'password' => $request->password], $remember)) {
                       $request->session()->regenerate();
                        return redirect()->route('dashboard.index');
                    }
                    else {
                        Alert::warning('Username atau Password Salah!', 'Coba Lagi!');
                        return view('auth.login');
                    }
                }else{
                    Alert::warning('Akun tidak terdaftar!', 'Silahkan Registrasi!');
                    return view('auth.login');
                }
            } catch (Exception $e) {
                throw $e;
                Alert::error('Server Error!', 'Harap Hubungi Tim IT!');
                return view('auth.login');
            };
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        Alert::success('Logout Success', 'Good Bye!');
        return redirect('/');
    }
}
