<?php

namespace App\Providers;

use Exception;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Exceptions\Handler;
use App\Helpers\LogApps;
use App\Models\KategoriDocument;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(Request $request)
    {
        View::composer('*', function ($view) {
            $categoryDocument = KategoriDocument::whereNotIn('id', [2])->get();
            $view->with('categoryDocument', $categoryDocument);
        });

        //Log Apps
        if ($request->method() != "GET") {
            LogApps::ActivityLog($request);
        }
    }
}
