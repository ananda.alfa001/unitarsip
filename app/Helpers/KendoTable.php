<?php
namespace App\Helpers;


class KendoTable {
    public static function KendoServerSide($model, $request) {
        //Get  Variable dari Kendo
        $take = $request->take;
        $skip = $request->skip;
        $page = $request->page;
        $pageSize = $request->pageSize;
        // Memproses permintaan data sesuai dengan parameter yang diberikan
        $db = $model::query();
        // Mengambil data dengan menggunakan metode pagination atau limit dan offset
        $db->when($page && $pageSize, function ($db) use ($page, $pageSize) {
            return $db->forPage($page, $pageSize);
        }, function ($db) use ($take, $skip) {
            return $db->take($take)->skip($skip);
        });
        $db->orderBy('id', 'DESC');
        $data = $db->get();
        // Menghitung total data untuk pengaturan paging
        $count = $model::count();
        //Formating by Kendo Laravel
        $fix = [
            'data' => $data,
            '_count' => $count,
        ];
        $jsonData = json_encode($fix, JSON_PRETTY_PRINT);
        $callback = request()->input('callback');
        $response = $callback . '(' .  $jsonData . ')';
        return response($response)->header('Content-Type', 'application/json');
    }
}
