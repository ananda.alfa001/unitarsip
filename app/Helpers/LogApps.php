<?php

namespace App\Helpers;

use App\Models\Log\LogActivity;
use Illuminate\Http\Request;

class LogApps
{
    public static function ActivityLog(Request $request)
    {
        //Prevent Data from Kendo
        if($request->has('models')){
            $data = $request->input('models');
        }else{
            $data = json_encode($request->all());
        }
        LogActivity::create([
            'subject' => 'UnitArsip' ?? '-',
            'url' => $request->fullUrl() ?? '-',
            'method' => $request->method() ?? '-',
            'agent' => $request->header('user-agent') ?? '-',
            'data' => $data, //Harus berupa Json
            'user_id' => auth()->check() ? auth()->user()->id : "Guest",
        ]);
    }
}
