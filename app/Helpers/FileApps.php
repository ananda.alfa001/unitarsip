<?php

namespace App\Helpers;

use App\Models\FileAttachment;
use App\Models\Log\LogActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class FileApps
{
    //FUNC File Attachment
    public static function InsertFileAttachment($FileRequest, $name, $description)
    {
        $file['user_id'] = Auth::user()->id ?? "guest";
        $file['description'] = $description;
       //Get Ext
       $file['ext'] = $FileRequest->getClientOriginalExtension();
       //Get Size
       $file['size'] = File::size($FileRequest);
       //Naming File (Bisa Disesuaikan berdasarkan Kebutuhan Penamaan)
       $file['title'] = Auth::user()->id . "_" . $name .'.'.$file['ext'];
       $file['location'] =  Auth::user()->id .'/'. $name;
       //Save file to Server
        $path = 'public/'.  Auth::user()->id .'/'. $name;
        $FileRequest->storeAs($path, $file['title']);
        //TABLE FILE_ATTACHMENT
        FileAttachment::create([
            'user_id'=> $file['user_id'],
            'title'=>$file['title'],
            'description'=>$file['description'],
            'location'=> $file['location'] ,
            'size'=>$file['size'],
            'ext'=> $file['ext'] ,
        ]);
    }
}
