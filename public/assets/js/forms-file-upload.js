/**
 * File Upload
 */

'use strict';

(function () {
    // previewTemplate: Updated Dropzone default previewTemplate
    // ! Don't change it unless you really know what you are doing
    const previewTemplate = `<div class="dz-preview dz-file-preview">
<div class="dz-details">
  <div class="dz-thumbnail">
    <img data-dz-thumbnail>
    <span class="dz-nopreview">No preview</span>
    <div class="dz-success-mark"></div>
    <div class="dz-error-mark"></div>
    <div class="dz-error-message"><span data-dz-errormessage></span></div>
    <div class="progress">
      <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" data-dz-uploadprogress></div>
    </div>
  </div>
  <div class="dz-filename" data-dz-name></div>
  <div class="dz-size" data-dz-size></div>
</div>
</div>`;

    // ? Start your code from here

    // Basic Dropzone
    // --------------------------------------------------------------------

    //   const myDropzone = new Dropzone('#dropzone-basic', {
    //     previewTemplate: previewTemplate,
    //     parallelUploads: 1,
    //     maxFilesize: 5,
    //     addRemoveLinks: true,
    //     maxFiles: 1
    //   });

    // Multiple Dropzone
    // --------------------------------------------------------------------
    const dropzoneMulti = new Dropzone('#dropzone-multi', {
        autoProcessQueue: true, // Set autoProcessQueue to true
        previewTemplate: previewTemplate,
        parallelUploads: 1,
        maxFilesize: 5,
        addRemoveLinks: true,
        init: function () {
            var dropzoneMulti = this;

            // After a file has been added to the queue
            this.on('addedfile', function (file) {
                // Get the values from the input fields
                var number = $('input[name="number"]').val();
                var desc = $('input[name="desc"]').val();
                // Check if the fields are empty
                if (number.trim() === '') {
                    $('#numberInput').addClass('is-invalid');
                } else {
                    $('#numberInput').removeClass('is-invalid');
                }
                if (desc.trim() === '') {
                    $('#descInput').addClass('is-invalid');
                } else {
                    $('#descInput').removeClass('is-invalid');
                }
                if (number.trim() === '' || desc.trim() === '') {
                    // Display a warning using SweetAlert
                    Swal.fire(
                        'Canceled!',
                        'Data Belum lengkap!',
                        'warning'
                    );
                    // Reset the Dropzone input
                    dropzoneMulti.removeAllFiles();
                    return; // Stop further processing
                }
                // Create a FormData object
                var formData = new FormData();
                formData.append('file', file);
                formData.append('number', number);
                formData.append('desc', desc);
                // Make an AJAX request to upload the file
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': csrf
                    }
                });
                $.ajax({
                    url: UrlUpload,
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        console.log('File uploaded successfully:', file);
                        console.log('Server response:', response);
                        // Handle the server response here
                    },
                    error: function (xhr, status, error) {
                        console.log('Error uploading file:', file);
                        console.log('Error:', error);
                        // Handle the error here
                    }
                });
            });
        }
    });
})();
