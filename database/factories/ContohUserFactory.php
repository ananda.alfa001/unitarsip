<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Contohuser;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ContohUser>
 */
class ContohUserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     *
     */
    protected $model = Contohuser::class;

    public function definition(): array
    {
        return [
            'nama' => $this->faker->name,
            'kekerabatan' => $this->faker->randomElement(['Ayah', 'Ibu', 'Anak', 'Saudara']),
            'nomor_hp' => $this->faker->randomNumber(5),
            'created_at' => now(),
            'updated_at' => now(),
        ];

    }
}
