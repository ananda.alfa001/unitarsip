<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PertanyaanSurveyTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('pertanyaan_survey')->delete();

        \DB::table('pertanyaan_survey')->insert(array (
            0 =>
            array (
                'id' => 1,
                'pertanyaan' => 'Bagaimana pendapat anda mengenai persyaratan pelayanan yang mudah dipahami dan dipenuhi sesuai dengan jenis pelayanannya ?',
                'type' => 1,
                'urutan' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'pertanyaan' => 'Bagaimana pendapat anda mengenai kejelasan informasi mekanisme dan prosedur pelayanan pada unit kerja ini?',
                'type' => 1,
                'urutan' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'pertanyaan' => 'Bagaimana pendapat anda mengenai kecepatan pelayanan sesuai dengan target waktu penyelesaian layanan?',
                'type' => 1,
                'urutan' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'pertanyaan' => 'Bagaimana pendapat anda mengenai kesesuaian waktu pelaksanaan layanan dengan waktu layanan?',
                'type' => 1,
                'urutan' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'pertanyaan' => 'Bagaimana pendapat anda mengenai kesesuaian produk layanan yang diberikan dengan ketentuan yang telah di tetapkan, seperti : surat tanda terima, surat rekomendasi, cuti akademik dan lain - lain ?',
                'type' => 1,
                'urutan' => 5,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'pertanyaan' => 'Bagaimana pendapat anda mengenai kemampuan petugas dalam memberikan pelayanan?',
                'type' => 1,
                'urutan' => 6,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'pertanyaan' => 'Bagaimana pendapat anda mengenai sikap petugas dalam memberikan pelayanan terkait dengan kesopanan dan keramahan ?',
                'type' => 1,
                'urutan' => 7,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'pertanyaan' => 'Bagaimana pendapat anda mengenai kejelasan tata cara penanganan pengaduan atas pelayanan?',
                'type' => 1,
                'urutan' => 8,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'pertanyaan' => 'Bagaimana pendapat anda mengenai kenyamanan ruang pelayanan?',
                'type' => 1,
                'urutan' => 9,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                'pertanyaan' => 'Bagaimana menurut anda mengenai ketersediaan buku tamu pada pelayanan?',
                'type' => 1,
                'urutan' => 10,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                'pertanyaan' => 'Bagaimana pendapat anda mengenai kemudahan akses website unit layanan yang ada ?',
                'type' => 1,
                'urutan' => 11,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            11 =>
            array (
                'id' => 12,
                'pertanyaan' => 'Bagaimana menurut anda mengenai kemudahan komunikasi melui sarana komunikasi, contoh:  telepon, email, media sosial',
                'type' => 1,
                'urutan' => 12,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            12 =>
            array (
                'id' => 13,
                'pertanyaan' => 'Berikan rating kepuasan atas layanan yang diterima ',
                'type' => 3,
                'urutan' => 13,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            13 =>
            array (
                'id' => 14,
                'pertanyaan' => 'Tuliskan kritik serta saran untuk meningkatkan kualitas layanan kami',
                'type' => 2,
                'urutan' => 14,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
        ));


    }
}
