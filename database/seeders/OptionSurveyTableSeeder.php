<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OptionSurveyTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('option_survey')->delete();
        
        \DB::table('option_survey')->insert(array (
            0 => 
            array (
                'id' => 1,
                'pertanyaan_id' => 1,
                'option' => 'tidak mudah',
                'point' => 0,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'pertanyaan_id' => 1,
                'option' => 'kurang mudah',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'pertanyaan_id' => 1,
                'option' => 'mudah',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'pertanyaan_id' => 1,
                'option' => 'cukup mudah',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'pertanyaan_id' => 1,
                'option' => 'sangat mudah',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'pertanyaan_id' => 2,
                'option' => 'tidak jelas',
                'point' => 0,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'pertanyaan_id' => 2,
                'option' => 'kurang jelas',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'pertanyaan_id' => 2,
                'option' => 'cukup jelas',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'pertanyaan_id' => 2,
                'option' => 'jelas',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'pertanyaan_id' => 2,
                'option' => 'sangat jelas',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'pertanyaan_id' => 3,
                'option' => 'tidak cepat',
                'point' => 0,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'pertanyaan_id' => 3,
                'option' => 'kurang cepat',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'pertanyaan_id' => 3,
                'option' => 'cepat',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'pertanyaan_id' => 3,
                'option' => 'cukup cepat',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'pertanyaan_id' => 3,
                'option' => 'sangat cepat',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'pertanyaan_id' => 4,
                'option' => 'tidak sesuai',
                'point' => 0,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'pertanyaan_id' => 4,
                'option' => 'kurang sesuai',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'pertanyaan_id' => 4,
                'option' => 'cukup sesuai',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'pertanyaan_id' => 4,
                'option' => 'sesuai',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'pertanyaan_id' => 4,
                'option' => 'sangat sesuai',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'pertanyaan_id' => 5,
                'option' => 'tidak sesuai',
                'point' => 0,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'pertanyaan_id' => 5,
                'option' => 'kurang sesuai',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'pertanyaan_id' => 5,
                'option' => 'cukup sesuai',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'pertanyaan_id' => 5,
                'option' => 'sesuai',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'pertanyaan_id' => 5,
                'option' => 'sangat sesuai',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'pertanyaan_id' => 6,
                'option' => 'tidak mampu',
                'point' => 0,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'pertanyaan_id' => 6,
                'option' => 'kurang mampu',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'pertanyaan_id' => 6,
                'option' => 'cukup mampu',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'pertanyaan_id' => 6,
                'option' => 'mampu',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'pertanyaan_id' => 6,
                'option' => 'sangat mampu',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'pertanyaan_id' => 7,
                'option' => 'tidak sopan dan tidak ramah',
                'point' => 0,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'pertanyaan_id' => 7,
                'option' => 'kurang sopan dan kurang ramah',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'pertanyaan_id' => 7,
                'option' => 'cukup sopan dan cukup ramah',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'pertanyaan_id' => 7,
                'option' => 'sopan dan ramah',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'pertanyaan_id' => 7,
                'option' => 'sangat sopan dan sangat ramah',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'pertanyaan_id' => 8,
                'option' => 'tidak jelas',
                'point' => 0,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'pertanyaan_id' => 8,
                'option' => 'kurang jelas',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'pertanyaan_id' => 8,
                'option' => 'cukup jelas',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'pertanyaan_id' => 8,
                'option' => 'jelas',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'pertanyaan_id' => 8,
                'option' => 'sangat jelas',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'pertanyaan_id' => 9,
                'option' => 'tidak nyaman',
                'point' => 0,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'pertanyaan_id' => 9,
                'option' => 'kurang nyaman',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'pertanyaan_id' => 9,
                'option' => 'cukup nyaman',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'pertanyaan_id' => 9,
                'option' => 'nyaman',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'pertanyaan_id' => 9,
                'option' => 'sangat nyaman',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'pertanyaan_id' => 10,
                'option' => 'tidak tersedia',
                'point' => 0,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'pertanyaan_id' => 10,
                'option' => 'kurang tersedia',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'pertanyaan_id' => 10,
                'option' => 'cukup tersedia',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'pertanyaan_id' => 10,
                'option' => 'tersedia',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'pertanyaan_id' => 10,
                'option' => 'sangat tersedia',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'pertanyaan_id' => 11,
                'option' => 'tidak ada',
                'point' => 0,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'pertanyaan_id' => 11,
                'option' => 'kurang mudah',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'pertanyaan_id' => 11,
                'option' => 'cukup mudah',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'pertanyaan_id' => 11,
                'option' => 'mudah',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'pertanyaan_id' => 11,
                'option' => 'sangat mudah',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'pertanyaan_id' => 12,
                'option' => 'tidak ada',
                'point' => 0,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'pertanyaan_id' => 12,
                'option' => 'ada tetapi tidak berfungsi',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'pertanyaan_id' => 12,
                'option' => 'berfungsi kurang maksimal',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'pertanyaan_id' => 12,
                'option' => 'dikelola dengan baik',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'pertanyaan_id' => 12,
                'option' => 'dikelola dengan sangat baik',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'pertanyaan_id' => 13,
                'option' => '1',
                'point' => 1,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'pertanyaan_id' => 13,
                'option' => '2',
                'point' => 2,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'pertanyaan_id' => 13,
                'option' => '3',
                'point' => 3,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'pertanyaan_id' => 13,
                'option' => '4',
                'point' => 4,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'pertanyaan_id' => 13,
                'option' => '5',
                'point' => 5,
                'created_at' => '2023-06-24 16:34:28',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}