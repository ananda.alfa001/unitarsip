<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert(array(
            0 =>
            array(
                'id' => 1,
                'name' => 'Super Admin',
                'email' => 'superadmin@arsip.ub.ac.id',
                'email_verified_at' => NULL,
                'verify_key' => Str::random(64),
                'password' => Hash::make('unitarsip11_'),
                'active' => 1,
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 =>
            array(
                'id' => 2,
                'name' => 'Harum Ninik Sugiharti',
                'email' => 'harumninik89@gmail.com',
                'email_verified_at' => NULL,
                'verify_key' => Str::random(64),
                'password' => Hash::make('harumcantik11'),
                'active' => 1,
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
    }
}
