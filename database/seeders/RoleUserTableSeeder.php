<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('role_user')->delete();

        \DB::table('role_user')->insert(array (
            0 =>
            array (
                'id' => 1,
                'user_id' => 1,
                'role_id' => 1,
                'created_at' => '2023-06-18 06:20:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'user_id' => 2,
                'role_id' => 1,
                'created_at' => '2023-06-18 06:20:33',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));


    }
}
