<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('jawaban_survey', function (Blueprint $table) {
            $table->id();
            $table->foreignId('penjawab_id')->constrained('penjawab_survey');
            $table->foreignId('pertanyaan_id')->constrained('pertanyaan_survey');
            $table->longText('jawaban')->index();
            $table->integer('point')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('jawaban_survey');
    }
};
