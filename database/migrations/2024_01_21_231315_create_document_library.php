<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('document_library', function (Blueprint $table) {
            $table->id();
            $table->longText('judul')->index();
            $table->string('document')->nullable();
            $table->longText('desc');
            $table->bigInteger('penglihat')->default(0);
            $table->foreignId('kategori_document_id')->constrained('kategori_document')->onUpdate('cascade')->onDelete('cascade');
            $table->boolean('publish')->default(0);
            $table->string('author')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('document_library');
    }
};
