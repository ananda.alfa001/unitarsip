<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\Menu\Berita\UpdateBerita\UpdateBeritaController;
use App\Http\Controllers\Menu\Berita\UpdateKategori\UpdateKategoriController;
use App\Http\Controllers\Menu\Dashboard\DashboardController;
use App\Http\Controllers\Menu\Dokumen\DokumenManagementController;
use App\Http\Controllers\Menu\Dokumen\DokumenManagementKategoriController;
use App\Http\Controllers\Menu\Dokumen\DokumenViewController;
use App\Http\Controllers\Menu\Survey\SurveyController;
use App\Http\Controllers\Menu\Survey\SurveyManagemenController;
use App\Http\Controllers\Menu\UserManagement\ListUser\ListUserController;
use App\Http\Controllers\Menu\UserManagement\UserPermission\UserPermissionController;
use App\Http\Controllers\Menu\UserManagement\UserRole\UserRoleController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use RealRashid\SweetAlert\Facades\Alert;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


/*
|--------------------------------------------------------------------------
| PROFILE
|--------------------------------------------------------------------------
*/
Route::get('/sejarah', function () {
    return view('home.homepage.profile.sejarah.index');
})->name('home.sejarah');
Route::get('/visi-misi', function () {
    return view('home.homepage.profile.visimisi.index');
})->name('home.visimisi');
Route::get('/organisasi', function () {
    return view('home.homepage.profile.struktur.index');
})->name('home.organisasi');
Route::get('/logo', function () {
    return view('home.homepage.profile.logo.index');
})->name('home.logo');


/*
|--------------------------------------------------------------------------
| JAMINAN MUTU
|--------------------------------------------------------------------------
*/
Route::get('/audit', function () {
    return view('home.homepage.jaminanmutu.audit.index');
})->name('home.audit');


/*
|--------------------------------------------------------------------------
| LAYANAN
|--------------------------------------------------------------------------
*/
Route::get('/pembinaan-kearsipan', function () {
    return view('home.homepage.layanan.pembinaanarsip.index');
})->name('home.pembinaan.kearsipan');
Route::get('/pengelolaan-kearsipan-statis', function () {
    return view('home.homepage.layanan.pengelolaanarsipstatis.index');
})->name('home.pengelolaan.kearsipan.statis');
Route::get('/pengelolaan-akuisisi-arsip', function () {
    return view('home.homepage.layanan.akuisisi.index');
})->name('home.pengelolaan.akuisisi.arsip');
Route::get('/pelayanan-peminjaman-arsip', function () {
    return view('home.homepage.layanan.peminjamanarsip.index');
})->name('home.pelayanan.peminjaman.arsip');
Route::get('/alihmedia-arsip', function () {
    return view('home.homepage.layanan.alihmedia.index');
})->name('home.alihmedia.arsip');
Route::get('/penyelamatan-arsip', function () {
    return view('home.homepage.layanan.perbaikanpelestarian.index');
})->name('home.penyelamatan.arsip');
Route::get('/pemusnahan-arsip', function () {
    return view('home.homepage.layanan.pemusnahan.index');
})->name('home.pemusnahan.arsip');
Route::get('/studi-banding', function () {
    return view('home.homepage.layanan.kunjungan.index');
})->name('home.stiudibanding.arsip');
Route::resource('survey', SurveyController::class);


/*
    |--------------------------------------------------------------------------
    | PRIVATE MENU PER ROLE
    | TINGKATAN DIMULAI PALING ATAS
    |--------------------------------------------------------------------------
*/
//AUTHENTICATION
Route::resource('/', HomeController::class);
Route::get('/news/{home}', [HomeController::class, 'show'])->name('home.show');

Alert::success('Selamat Datang di Arsip UB Apps!');
Auth::routes();

//Super Admin
Route::prefix('')->middleware(['auth', 'role:Super Admin'])->group(function () {
    //
});

//Admin Head Office,
Route::prefix('')->middleware(['auth', 'role:Super Admin, Admin Head Office'])->group(function () {
    //
});

//Admin Company

Route::prefix('')->middleware(['auth', 'role:Super Admin, Admin Head Office, Admin Company'])->group(function () {
    /*
    |--------------------------------------------------------------------------
    | User Management Resource Routes
    |--------------------------------------------------------------------------
    */
    //User List
    Route::post('user-list/company', [ListUserController::class, 'create_company'])->name('create-company');
    Route::get('user-list/list', [ListUserController::class, 'index_datatable'])->name('index-user');
    Route::resource('user-list', ListUserController::class);
    //User Role
    Route::resource('user-role', UserRoleController::class);
    //User Permission
    Route::resource('user-permission', UserPermissionController::class);
    /*
    |--------------------------------------------------------------------------
    | Next Resource Routes
    |--------------------------------------------------------------------------
    */
});

//User (Menu '/' dibuat di tingkatan paling rendah, karena default access ke /)
Route::prefix('')->middleware(['auth', 'role:Super Admin, Admin Head Office, Admin Company, User'])->group(function () {
    /*
    |--------------------------------------------------------------------------
    | Dashboard Resource Routes
    |--------------------------------------------------------------------------
    */
    Route::resource('dashboard', DashboardController::class);
    /*
    |--------------------------------------------------------------------------
    | File Resource Kategori
    |--------------------------------------------------------------------------
    */
    Route::resource('kategori-berita', UpdateKategoriController::class);
    /*
    |--------------------------------------------------------------------------
    | File Resource Berita
    |--------------------------------------------------------------------------
    */
    Route::get('berita/list', [UpdateBeritaController::class, 'index_datatable'])->name('index-berita');
    Route::resource('berita', UpdateBeritaController::class);
    /*
    |--------------------------------------------------------------------------
    | File Resource Management Dokumen
    |--------------------------------------------------------------------------
    */
    Route::resource('dokumen-management', DokumenManagementController::class);
    /*
    |--------------------------------------------------------------------------
    | File Resource Kategori Management Dokumen
    |--------------------------------------------------------------------------
    */
    Route::resource('kategori-dokumen-management', DokumenManagementKategoriController::class);
    /*
    |--------------------------------------------------------------------------
    | Survey
    |--------------------------------------------------------------------------
    */
    Route::resource('survey-management', SurveyManagemenController::class);
});

/*
|--------------------------------------------------------------------------
| File Resource Dokumen
|--------------------------------------------------------------------------
*/
Route::resource('dokumen', DokumenViewController::class);
